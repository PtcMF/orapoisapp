import React, { useState, useEffect } from 'react'
import { Image, ImageBackground, TouchableOpacity } from 'react-native';
import { Container, Header, Text, View, Item, Input, Icon, Button, Form, Tabs, Tab, TabHeading } from 'native-base';
import * as Font from 'expo-font';
import { saveLoggedUser, getLoggedUser, validateCPF, getCredencialByEmail, sendProfileServer, logout, getUrlNode } from '../controllers/Loginctrl'
import Swipeable from 'react-native-gesture-handler/Swipeable'
import validate from 'validate.js'
import PV from '../components/PraeValidation'
import { TextInputMask } from 'react-native-masked-text'
import { showMessage } from '../components/Utils';
import HeaderComprae from '../components/HeaderComprae';
import commonStyles from '../commonStyles';
import { FlatList } from 'react-native-gesture-handler';
import imgLambreta from '../../assets/imgs/lambreta.jpeg'
import logoComprae from '../../assets/imgs/logo_comprae.png'
import { callEditAddress, callDeleteAddress, callSelectedAddress, getServerAddresses } from '../controllers/Enderecoctrl'
import { getSelectedMarket } from '../controllers/Mercadoctrl';

const Profile = ({ navigation }) => {
    const [user, setUser] = useState({});
    const [fontLoaded, setFontLoaded] = useState(false);
    const [addresses, setAddresses] = useState([]);

    //EVENTOS
    let carregarFonts = async () => {
        await Font.loadAsync({
            Roboto: require("native-base/Fonts/Roboto.ttf"),
            Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
            Lato: require("../../assets/fonts/Lato.ttf")
        });
        setFontLoaded(true);
    }
    useEffect(() => {
        carregarFonts();
    }, []);

    getUser = async () => {
        console.log('XXXXXXXXXXXXXX getUser XXXXXXXXXXXXXXXXXXX')
        let userLogged = await getLoggedUser();        
        if (!userLogged.idpessoa) {
            let userServer = await getCredencialByEmail(userLogged.email)            
            if (userServer) {
                userLogged['nome'] = userServer.nome
                userLogged['cnpjf'] = userServer.cnpjf
                userLogged['celular'] = userServer.celular
                userLogged['idpessoa'] = userServer.idpessoa
                let _addresses = await getServerAddresses(userLogged);
                if (_addresses && _addresses.length > 0) {
                    userLogged.addresses = _addresses                    
                    userLogged['defaultAddress'] = _addresses[0]
                    userLogged['selectedAddress'] = _addresses[0]

                }
                saveLoggedUser(userLogged)
            }
        }
        if (!user.email) {
            setUser(userLogged)
        }
        if (user.addresses) {
            setAddresses(user.addresses)
        }
    }
    getUser();
    validateForm = async () => {
        console.log('VALIDATE FORM')
        let valResult = validate(user, PV)
        let fields = ['nome', 'e-mail', 'celular']
        let concluido = true
        fields.forEach(element => {
            console.log(element)
            if (valResult[element] && concluido === true) {
                showMessage(valResult[element][0])
                concluido = false
            }
        });
        if (concluido === false) {
            return
        }
        if (await validateCPF(user.cnpjf) === false) {
            showMessage('CPF inválido')
            return
        }
        let serverUser = await sendProfileServer(user)
        saveLoggedUser(user)
        if (navigation.state.params && navigation.state.params.back) {
            navigation.goBack()
        } else {
            let _market = await getSelectedMarket();
            if (_market && _market.descricao) {
                navigation.navigate('FinalizarCompra')
            } else {
                navigation.navigate('Mercados')
            }
        }
        return true
    }
    refresh = () => {
        console.log('REFRESH MEUS ENDERECOS')
        setTimeout(() => {
            console.log('REFRESH MEUS ENDERECOS 66666')
            setUser({})
            setAddresses(null)
        }, 200)

    }
    //ENDEREÇOS    
    newAddress = () => {
        callEditAddress({}, navigation, refresh)
    }
    editAddress = (address) => {
        callEditAddress(address.item, navigation, refresh)
    }
    deleteAddress = async (address) => {
        await callDeleteAddress(address.item, user)
        refresh()
    }
    const GetRightContent = ({ progress, item }) => {
        return (
            <View>
                <TouchableOpacity style={{
                    backgroundColor: commonStyles.colors.grayBackground, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end',
                    paddingHorizontal: 20, flex: 5, borderBottomWidth: 1
                }}
                    onPress={() => { editAddress({ item }) }}>
                    <Icon name="md-create" size={30} color='#FFF' />
                </TouchableOpacity>
                <TouchableOpacity style={{
                    backgroundColor: commonStyles.colors.grayBackground, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end',
                    paddingHorizontal: 20, flex: 5, borderBottomWidth: 1
                }}
                    onPress={() => { deleteAddress({ item }) }}>
                    <Icon name="ios-trash" size={30} color='#FFF' />
                </TouchableOpacity>
            </View>
        )
    }
    console.log('Profile XXXXXXXXX')
    console.log(user)
    if (fontLoaded === false) {
        return (<Text>aaaa</Text>)
    }
    return (
        <Container>
            <HeaderComprae hidelogo showbackicon navigation={navigation}
                container={<Image source={logoComprae}
                    style={{ marginLeft: -15, width: 70, height: 30, alignSelf: 'center', resizeMode: 'contain' }} />}
            ></HeaderComprae>
            <View style={{ flex: 1, justifyContent: 'center', backgroundColor: commonStyles.colors.grayBackground }}>
                <View style={{ flexDirection: 'row', flex: 2, justifyContent: 'center' }}>
                    <View style={{ justifyContent: 'center' }}>
                        <Image
                            source={{ uri: (user.picture) ? user.picture : 'https://cdn3.iconfinder.com/data/icons/abstract-1/512/no_image-512.png' }}
                            style={{ width: 120, height: 120, borderRadius: 75 }}
                        />
                    </View>
                </View>
                <View style={{ backgroundColor: 'white', height: 10 }}></View>
                <Tabs style={{ flex: 8 }} tabBarUnderlineStyle={{ borderBottomWidth: 2 }}>
                    <Tab
                        tabStyle={{ backgroundColor: commonStyles.colors.grayBackground }} textStyle={{ color: 'black' }} activeTabStyle={{ backgroundColor: commonStyles.colors.grayBackground }} activeTextStyle={{ color: 'black', fontWeight: 'normal' }}
                        heading='MEU PERFIL'>

                        <View style={{ flex: 5, padding: 40 }}>
                            <Form style={{ justifyContent: 'space-between' }}>
                                <Item color={commonStyles.colors.iconColor}>
                                    <Icon name='ios-person' style={{ color: commonStyles.colors.iconColor }} />
                                    <Input style={{ color: commonStyles.colors.iconColor }} placeholderTextColor={commonStyles.colors.iconColor} placeholder='Nome'
                                        value={user.nome}
                                        onChangeText={(text) => {
                                            let tmpUser = JSON.parse(JSON.stringify(user))
                                            tmpUser.nome = text
                                            setUser(tmpUser)
                                        }} />
                                </Item>
                                <Item color={commonStyles.colors.iconColor}>
                                    <Icon name='ios-mail' style={{ color: commonStyles.colors.iconColor }} />
                                    <Input style={{ color: commonStyles.colors.iconColor }} placeholderTextColor={commonStyles.colors.iconColor} placeholder='e-mail'
                                        value={user.email} />
                                </Item>
                                <Item color={commonStyles.colors.iconColor} style={{ marginTop: 12, paddingBottom: 10 }}>
                                    <Icon name='ios-call' style={{ color: commonStyles.colors.iconColor }} />
                                    <TextInputMask style={{ color: commonStyles.colors.iconColor, fontSize: 17, marginLeft: 5, flex: 10 }} placeholderTextColor={commonStyles.colors.iconColor} placeholder='Celular'
                                        type={'cel-phone'}
                                        options={{
                                            maskType: 'BRL',
                                            withDDD: true,
                                            dddMask: '(99) '
                                        }}
                                        value={user.celular}
                                        onChangeText={text => {
                                            let tmpUser = JSON.parse(JSON.stringify(user))
                                            tmpUser.celular = text
                                            setUser(tmpUser)
                                        }}
                                    />
                                </Item>
                                <Item color={commonStyles.colors.iconColor} style={{ marginTop: 12, paddingBottom: 10 }}>
                                    <Icon name='ios-paper' style={{ color: commonStyles.colors.iconColor }} />
                                    <TextInputMask style={{ color: commonStyles.colors.iconColor, fontSize: 17, marginLeft: 5, flex: 10 }} placeholderTextColor={commonStyles.colors.iconColor} placeholder='CPF'
                                        type={'cpf'}
                                        options={{
                                            maskType: 'BRL',
                                        }}
                                        value={user.cnpjf}
                                        onChangeText={text => {
                                            let tmpUser = JSON.parse(JSON.stringify(user))
                                            tmpUser.cnpjf = text
                                            setUser(tmpUser)
                                        }}
                                    />
                                </Item>
                            </Form>
                            <View style={{ justifyContent: 'center', marginTop: 70 }}>
                                <Button style={{ justifyContent: 'center', backgroundColor: commonStyles.colors.buttonGreen, borderRadius: 5 }}
                                    onPressOut={() => validateForm()}>
                                    <Text style={{ textAlign: 'center' }}>CONCLUIR</Text>
                                </Button>
                                <Button style={{ justifyContent: 'center', backgroundColor: commonStyles.colors.buttonGreen, borderRadius: 5, marginTop: 30 }}
                                    onPressOut={() => logout(navigation)}>
                                    <Text style={{ textAlign: 'center' }}>LOGOUT</Text>
                                </Button>
                            </View>
                        </View>

                    </Tab>                    
                    <Tab
                        tabStyle={{ backgroundColor: commonStyles.colors.grayBackground }} textStyle={{ color: 'black' }} activeTabStyle={{ backgroundColor: commonStyles.colors.grayBackground }} activeTextStyle={{ color: 'black', fontWeight: 'normal' }}
                        heading='ENDEREÇOS' {...(user.idpessoa? undefined : {disabled:true}) } 
                        >
                        <View style={{ flex: 90 }}>
                            <FlatList data={addresses}
                                keyExtractor={item => `${item.numero}`}
                                renderItem={({ item }) => {
                                    return (
                                        <Swipeable renderRightActions={(progress, dragX) => (
                                            <GetRightContent progress={progress} dragX={dragX} item={item} />
                                        )}>
                                            <View style={{
                                                flexDirection: 'row', borderColor: '#AAA', borderBottomWidth: 1, borderColor: commonStyles.colors.grayHeader,
                                                paddingVertical: 10, marginTop: 10
                                            }}>
                                                <Image
                                                    source={imgLambreta}
                                                    style={{ marginLeft: 10, width: 60, height: 60, borderRadius: 75 }}
                                                />
                                                <View style={{ flex: 6, marginRight: 15, marginLeft: 10, top: 2, borderColor: 'black', borderWidth: 0, justifyContent: 'space-between' }}>
                                                    <Text style={{ marginLeft: 15, fontSize: 13 }}>{item.endereco}, {item.numero}, {item.referencia} </Text>
                                                    <Text style={{ marginLeft: 15, fontSize: 13 }}>{item.bairro}</Text>
                                                    <Text style={{ marginLeft: 15, fontSize: 13 }}>{item.city.cidade} - {item.uf.siglauf}</Text>
                                                </View>
                                            </View>
                                        </Swipeable>
                                    );
                                }}>
                            </FlatList>
                        </View>
                        <Button style={{ marginBottom: 30, marginLeft: 5, marginRight: 5, justifyContent: 'center', backgroundColor: commonStyles.colors.buttonGreen }}
                            onPress={() => {
                                newAddress();
                            }}>
                            <Text>Adicionar Endereço</Text>
                        </Button>
                    </Tab>
                    <Tab
                        tabStyle={{ backgroundColor: commonStyles.colors.grayBackground }} textStyle={{ color: 'black' }} activeTabStyle={{ backgroundColor: commonStyles.colors.grayBackground }} activeTextStyle={{ color: 'black', fontWeight: 'normal' }}
                        heading='PAGAMENTO' {...(user.idpessoa? undefined : {disabled:true}) } >
                    </Tab>
                </Tabs>

            </View>
        </Container >
    );
}
Profile.navigationOptions = {
    title: 'Profile',
    headerShown: false
}

export default Profile