import React, { useState, useEffect } from 'react'
import { AsyncStorage } from 'react-native';
import { FlatList, SafeAreaView, Image, TouchableOpacity } from 'react-native'
import { Container, Text, View } from 'native-base';
import DefaultStyle from '../style/DefaultStyle'
import * as Font from 'expo-font';
import Icon from 'react-native-vector-icons/FontAwesome'
import Loading from '../components/Loading';
import HeaderComprae from '../components/HeaderComprae';
import commonStyles from '../commonStyles';
import { showProfileScreen, changeServer} from '../controllers/Loginctrl';
import { buscarProdutos, clearCarrinho } from '../controllers/Produtosctrl'
import { getMarketList, getStatusMarket, getSelectedMarket } from '../controllers/Mercadoctrl';
import { showMessage } from '../components/Utils';


const Mercados = ({ navigation }) => {
    // console.log('TELA DE MERCADOS')
    const [mercados, setMercados] = useState(null);
    const [fontLoaded, setFontLoaded] = useState(false);
    const [timeRefresh, setTimeRefresh] = useState(null);    

    let carregarFonts = async () => {
        await Font.loadAsync({
            Lato: require("../../assets/fonts/Lato.ttf"),
            Segoe: require("../../assets/fonts/segoe-ui.otf")
        });
        setFontLoaded(true)
    }
    useEffect(() => {
        carregarFonts();
    }, []);
    if (!fontLoaded) {
        return <Loading></Loading>
    }
    let loadMarkets = async () => {
        let arr = await getMarketList();
        setMercados(arr)
    }
    if (mercados === null) {
        loadMarkets();
        jsxLoading = <Loading></Loading>
    }
    let checkMarketStatus = async () => {        
        if(!mercados)
        {
            return
        }
        let str = JSON.stringify(mercados)
        let obj = JSON.parse(str)
        for (let i = 0; i < mercados.length; i++) {            
            let status = await getStatusMarket(mercados[i])
            obj[i].status = status
            if(status.freteapp)
            {
                obj[i].freteapp = status.freteapp
            }
        }
        setTimeout(() => {
            setTimeRefresh(false)
        }, 30000)
        setMercados(obj)
    }
    
    let jsxLoading = null;
    let searchProdutos = async (filtro) => {
        let prods = await buscarProdutos(filtro);
        setProdutos(prods);
    }
    onSelectMarket = async (market) => {
        let saveMarket = await getSelectedMarket()        
        if (saveMarket) {
            if (saveMarket.estab != market.estab) {
                clearCarrinho()
            }
        }
        AsyncStorage.setItem('market', JSON.stringify(market));
        navigation.navigate('Products')
    }
    if (!timeRefresh && mercados) {
        checkMarketStatus()
        setTimeRefresh(true)
    }
    if (!mercados || mercados.length <= 0) {
        jsxLoading = <View></View>        
    } 
    let headerIcons = [{ name: 'ios-pin', onPress: () => { 
        let ischanged = changeServer()
        console.log('ischanged ' + ischanged)
        console.log(ischanged)
        if(ischanged == true)
        {
            setMercados(null)
        }
     } },
    { name: 'ios-person', onPress: () => { showProfileScreen(navigation) } }]
    return (
        <Container>
            <HeaderComprae icons={headerIcons} backgroundColor={'#FFF'} fontfamily={'Segoe'}
                title='ESTABELECIMENTOS'></HeaderComprae>
            {(jsxLoading) ? jsxLoading :
                (<SafeAreaView style={DefaultStyle.top10, { flex: 9, backgroundColor: commonStyles.colors.grayBackground }}>
                    <FlatList
                        data={mercados}
                        keyExtractor={(item, index) => {
                            return index.toString();
                        }}
                        renderItem={({ item, index }) => {
                            return (
                                <TouchableOpacity key={index} onPress={() => onSelectMarket(item)} disabled={!item.status}>

                                    <View style={{
                                        paddingHorizontal: 15, height: 100, backgroundColor: '#fbfbfb', flexDirection: 'row',
                                        marginTop: 10, marginLeft: 10, marginRight: 10, borderRadius: 15
                                    }}>
                                        <Image
                                            style={{ flex: 4, height: 70, alignSelf: "center", resizeMode: 'contain' }}
                                            source={{ uri: item.logo }}
                                        />
                                        <View style={{ marginLeft: 10, justifyContent: 'space-between', flex: 6 }}>
                                            <Text style={{ marginTop: 20, marginLeft: 10, marginRight: 5, fontSize: 14, fontWeight: 'bold' }}>{item.fantasia}</Text>
                                            <View style={{ flexDirection: 'row', bottom: 10, marginLeft: 10, marginRight: 5 }}>
                                                <Icon name="circle" size={20} color={(item.status) ? 'green' : 'gray'} />
                                                <Text style={{ marginLeft: 10 }}>{(item.status) ? 'Aceitando Pedidos' : 'Indisponível'}</Text>
                                            </View>
                                        </View>
                                        <Icon name="chevron-right" size={15} color='black' style={{ alignSelf: 'center', position: 'absolute', right: 1, opacity: 0.3 }} />
                                    </View>
                                </TouchableOpacity>
                            );
                        }}
                    />
                </SafeAreaView>)}
        </Container >
    )
}
Mercados.navigationOptions = {
    title: 'Mercados',
    headerShown: false
}
export default Mercados