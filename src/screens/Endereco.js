import React, { useState, useEffect } from 'react'
import { Image, ImageBackground, TouchableOpacity } from 'react-native';
import { Container, Header, Text, View, Item, Input, Icon, Button, Form, Picker } from 'native-base';
import { TextInputMask } from 'react-native-masked-text'
import * as Font from 'expo-font';
import validate from 'validate.js'
import Loading from '../components/Loading';
import HeaderComprae from '../components/HeaderComprae';
import commonStyles from '../commonStyles';
import { saveLoggedUser, getLoggedUser } from '../controllers/Loginctrl'
import { getUFs, getCities, getEditAddress, sendAddressServer } from '../controllers/Enderecoctrl'
const Endereco = ({ navigation }) => {
    const [user, setUser] = useState({});
    const [fontLoaded, setFontLoaded] = useState(false);
    const [address, setAddress] = useState({ uf: {}, city: {} });
    const [ufs, setUfs] = useState(null);
    const [jufs, setJufs] = useState([]);
    const [cities, setCities] = useState([]);
    const [jcities, setJcities] = useState([]);

    //EVENTOS
    let carregarFonts = async () => {
        await Font.loadAsync({
            Roboto: require("native-base/Fonts/Roboto.ttf"),
            Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
            Lato: require("../../assets/fonts/Lato.ttf")
        });
        setFontLoaded(true);
    }
    useEffect(() => {
        carregarFonts();
    }, []);
    updateAddress = (field, text) => {
        let addressTmp = JSON.parse(JSON.stringify(address))
        if (!addressTmp.id) {
            addressTmp['id'] = (Math.random() * (9999 - 1) + 1).toFixed(0)
        }
        addressTmp[field] = text
        setAddress(addressTmp)
    }
    getUser = async () => {
        if (!user.email) {
            let userLogged = await getLoggedUser();
            setUser(userLogged)
        }
    }
    getUser();
    loadEditAddress = async () => {
        if (user.email && !address.uf.uf) {
            tmpAddress = await getEditAddress()
            console.log('loadEdit')
            console.log(tmpAddress)
            if (tmpAddress && tmpAddress.uf) {
                setAddress(tmpAddress)
            }
        }
    }
    loadEditAddress()
    let validacoes = {
        endereco: {
            presence: {
                message: '^Insira um endereço'
            },
            length: {
                minimum: 5,
                message: 'muito curto'
            }
        },
        numero: {
            presence: {
                message: '^Insira o número da rua'
            }
        },
        referencia: {
            presence: {
                message: '^Insira uma referência'
            }
        },
        bairro: {
            presence: {
                message: '^Insira um bairro'
            },
            length: {
                minimum: 5,
                message: ' muito curto'
            }
        },
        cep: {
            presence: {
                message: '^Insira um cep'
            },
            length: {
                minimum: 9,
                message: ' inválido'
            }
        },
    }
    validateForm = async () => {
        console.log('VALIDATE FORM')
        console.log(address)
        if (!address.uf.uf || address.uf.uf === '0') {
            alert('Selecione um estado')
            return
        }
        if (!address.city.idcidade || address.city.idcidade === '0') {
            alert('Selecione uma cidade')
            return
        }
        let valResult = validate(address, validacoes)
        let fields = ['cep', 'bairro', 'endereco', 'numero', 'referencia']
        let concluido = true
        if (valResult) {
            fields.forEach(element => {
                if (concluido) {
                    if (valResult[element]) {
                        alert(valResult[element][0])
                        concluido = false
                    }
                }
            });
        }
        if (concluido === false) {
            return
        }
        console.log('Salvar endereço')
        let tmpUser = JSON.parse(JSON.stringify(user))
        if(!tmpUser.idpessoa)
        {
            alert('Cadastro Usuário imcompleto')
            return
        }
        if (!tmpUser['addresses']) {
            tmpUser['addresses'] = []
            tmpUser['defaultAddress'] = address
            tmpUser['selectedAddress'] = address
        }
        await saveAddress(tmpUser, address)
        return true
    }
    saveAddress = async (tmpUser, tmpAddress) => {
        console.log('saveAddress')
        // let enderecoServer = tmpAddress
        let enderecoServer = await sendAddressServer(tmpUser, tmpAddress)
        if (!enderecoServer) {
            return
        }
        if (user.addresses) {
            for (var i = 0; i < user.addresses.length; i++) {
                if (user.addresses[i].id === tmpAddress.id) {
                    tmpUser.addresses.splice(i, 1)
                    break
                }
            }
        }
        tmpUser['addresses'].push(enderecoServer)
        console.log('ATUALIZOU ENDERECO')
        console.log(user.addresses)
        saveLoggedUser(tmpUser)
        if (navigation.state.params && navigation.state.params.refresh) {
            navigation.state.params.refresh()
        }
        navigation.goBack();
    }
    console.log('Profile XXXXXXXXX')
    console.log(address)
    getUF = async () => {

        if (ufs == null) {
            let res = await getUFs()
            let arrUFs = res;
            let arrJsxUfs = []
            let jUf = {}
            if (arrUFs == null || arrUFs.length <= 0) {
                navigation.goBack();
            }
            arrUFs.forEach(element => {
                jUf[element['uf']] = element
                arrJsxUfs.push(<Picker.Item key={element['uf']} label={element['nomeuf']} value={element['uf']} />)
            });
            setUfs(arrJsxUfs);
            setJufs(jUf)

        }
    }
    getUF()
    getCities1 = async () => {
        if (address.uf.uf && cities.length <= 0) {
            let res = await getCities(address.uf.uf)
            let arrJsxCities = []
            let jCities = {}
            res.forEach(element => {
                jCities[element['idcidade']] = element;
                arrJsxCities.push(<Picker.Item key={element['idcidade']} label={element['cidade']} value={element['idcidade']} />)
            });
            setCities(arrJsxCities);
            setJcities(jCities);
        }
    }
    getCities1();
    if (fontLoaded === false || ufs == null || ufs.length <= 0) {

        return (<Loading></Loading>)
    }
    console.log('XXXXXXXXXXXXXXXXXXXXXXXXXXXXX ENDEREÇO XXXXXXXXXXXXXXXXXXXXXXXX')
    console.log(navigation)
    return (
        <Container>
            <HeaderComprae hidelogo title='Endereço'></HeaderComprae>
            <View style={{ flex: 9, justifyContent: 'center' }}>
                <View style={{ flex: 5, padding: 40 }}>
                    <Form style={{ justifyContent: 'space-between' }}>
                        <View style={{ flexDirection: 'column' }}>
                            <Text style={{}}>Estado</Text>
                            <Picker
                                mode="dropdown"
                                iosIcon={<Icon name="arrow-down" />}
                                style={{}}
                                placeholder=""
                                placeholderStyle={{ color: "#bfc6ea" }}
                                placeholderIconColor="#007aff"
                                selectedValue={address.uf.uf}
                                onValueChange={(value) => {
                                    setCities([])
                                    updateAddress('uf', jufs[value])
                                }}
                            >
                                {ufs}
                            </Picker>
                            <Text style={{}}>Cidade</Text>
                            <Picker
                                mode="dropdown"
                                iosIcon={<Icon name="arrow-down" />}
                                style={{}}
                                placeholder="Select your SIM"
                                placeholderStyle={{ color: "#bfc6ea" }}
                                placeholderIconColor="#007aff"
                                selectedValue={address.city.idcidade}
                                onValueChange={(value) => {
                                    console.log(jcities)
                                    let city = jcities[value]
                                    console.log(city)
                                    updateAddress('city', city)
                                }}
                            >
                                {cities}
                            </Picker>


                            <Item>
                                <TextInputMask
                                    style={{ fontSize: 17, marginLeft: 5, flex: 10 }}
                                    keyboardType='decimal-pad'
                                    placeholder='cep'
                                    type={'zip-code'}
                                    value={address.cep}
                                    onChangeText={(text) => { updateAddress('cep', text) }}
                                />
                            </Item>
                            <Item>
                                <Input style={{}} placeholder='bairro'
                                    value={address.bairro}
                                    onChangeText={(text) => { updateAddress('bairro', text) }} />
                            </Item>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <Item style={{ flex: 6 }}>
                                <Input style={{}} placeholder='rua'
                                    value={address.endereco}
                                    onChangeText={(text) => { updateAddress('endereco', text) }} />
                            </Item>
                            <Item style={{ flex: 2, marginLeft: 15 }}>
                                <Input style={{}} placeholder='número'
                                    keyboardType='decimal-pad'
                                    value={address.numero}
                                    onChangeText={(text) => { updateAddress('numero', text) }} />
                            </Item>
                        </View>
                        <View style={{}}>
                            <Item>
                                <Input style={{}} placeholder='referencia'
                                    value={address.referencia}
                                    onChangeText={(text) => { updateAddress('referencia', text) }} />
                            </Item>
                            <Item>
                                <Input style={{}} placeholder='complemento'
                                    value={address.complemento}
                                    onChangeText={(text) => { updateAddress('complemento', text) }} />
                            </Item>
                        </View>
                    </Form>
                </View>
                <View style={{ justifyContent: 'center', marginBottom: 20, marginLeft: 10, marginRight: 10 }}>
                    <Button style={{ justifyContent: 'center', backgroundColor: commonStyles.colors.buttonGreen, borderRadius: 5 }}
                        onPressOut={() => validateForm()}>
                        <Text style={{ textAlign: 'center' }}>CONCLUIR</Text>
                    </Button>
                </View>
            </View>
        </Container>
    );
}
Endereco.navigationOptions = {
    title: 'Endereco',
    headerShown: false
}

export default Endereco