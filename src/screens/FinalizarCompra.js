import React, { useState, useEffect } from 'react'
import { StyleSheet, ImageBackground, Image, TouchableOpacity } from 'react-native'
import { Container, ListItem, Radio, CheckBox, Left, Right, Button, Text, View } from 'native-base';
import FooterTabs from '../components/FooterTabs';
import * as Font from 'expo-font';
import commonStyles from '../commonStyles'
import { AsyncStorage } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons'
import Wait from '../components/Wait';
import { ShowMessage1 } from '../components/Utils'
import HeaderComprae from '../components/HeaderComprae';
import { getLoggedUser } from '../controllers/Loginctrl'
import { enviarPedido, clearCarrinho } from '../controllers/Produtosctrl'
import { getSelectedMarket } from '../controllers/Mercadoctrl';
const FinalizarCompra = ({ navigation }) => {
    const [fontLoaded, setFontLoaded] = useState(false);
    const [frete, setFrete] = useState(5);
    const [pagamento, setPagamento] = useState('dinheiro');
    const [retirar, setRetirar] = useState(false);
    const [user, setUser] = useState(null);
    const [pedido, setPedido] = useState(null);
    const [address, setAddress] = useState(null);
    const [showWait, setShowWait] = useState(false);
    const [showMessagePurchase, setShowMessagePurchase] = useState(false);
    const [market, setMarket] = useState(null);
    let pedidoTemp = {};

    let carregarFonts = async () => {
        await Font.loadAsync({
            Roboto: require("native-base/Fonts/Roboto.ttf"),
            Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
            Lato: require("../../assets/fonts/Lato.ttf")
        });
        setFontLoaded(true);
    }
    useEffect(() => {
        carregarFonts();
    }, []);
    console.log('FINALIZAR XXXXXXXXX')
    // console.log(user)
    // console.log(pedido)
    let loadUser = async (force) => {
        if (user === null || force === true) {
            let usr = await getLoggedUser()
            setUser(usr)
        }
    }
    loadUser()
    sendPurchaseOrder = async () => {
        console.log('finalizar pedido')
        setShowWait(true)
        let enviado = await enviarPedido(pedido, pagamento, retirar, user)
        console.log(enviado)
        if (enviado === true) {
            setShowMessagePurchase(true)
            setTimeout(() => {
                navigation.navigate('MeusPedidos', { refresh: true })
                setPedido(null)
                setShowMessagePurchase(false)
            }, 3000)
            await clearCarrinho();
            if (navigation.state.params['refresh']) {
                navigation.state.params.refresh()
            }
        }
        setShowWait(false)
    }
    if (!fontLoaded) {
        return <Wait visible={showWait}></Wait>
    }

    //CARREGAR LISTA DE PRODUTOS DO CARRINHO
    let valorTotalCompra = 0.0
    let carregarProdutos = () => {
        try {
            AsyncStorage.getItem('carrinho', (err, result) => {
                let carrinho = {};
                if (result) {
                    carrinho = JSON.parse(result);
                }
                let prods = [];
                for (var [key, value] of Object.entries(carrinho)) {
                    value["editar"] = false;
                    let totalProdutos = value.quantidade * value.precovarejo
                    valorTotalCompra = valorTotalCompra + totalProdutos
                    prods.push(value)
                }
                pedidoTemp['produtos'] = prods
                pedidoTemp['qtdeItens'] = prods.length
                pedidoTemp['valorprodutos'] = valorTotalCompra
                pedidoTemp['valortotal'] = valorTotalCompra + (market ? parseFloat(market.freteapp) : 0.00)
                setPedido(pedidoTemp)
            })

        } catch (error) {
            console.log('ERROR')
            console.log(error)
        }
    }
    carregarMercado = async () => {
        let _market = await getSelectedMarket();
        if (_market) {
            setMarket(_market)
        }
    }
    if (market == null) {
        carregarMercado()
        return <Wait visible={showWait}></Wait>
    }
    if (pedido === null) {
        carregarProdutos();
        return <Wait visible={showWait}></Wait>
    }
    let endereco;
    if (!pedido.enderecoEntrega) {
        if (user.selectedAddress) {
            endereco = (<View>
                <Text style={{ marginLeft: 15, fontSize: 15 }}>{user.selectedAddress.endereco}, {user.selectedAddress.numero}, {user.selectedAddress.referencia} </Text>
                <Text style={{ marginLeft: 15, fontSize: 15 }}>{user.selectedAddress.bairro}</Text>
                <Text style={{ marginLeft: 15, fontSize: 15 }}>{user.selectedAddress.city.cidade} - {user.selectedAddress.uf.siglauf}</Text>
                {/* <Text style={{ marginLeft: 15, fontSize: 15 }}>CEP: 85605-270</Text> */}
            </View>);
        } else {
            endereco = (<View></View>);
        }
    }
    console.log('XXXXXXXXXXXXXXXXXX PEDIDO XXXXXXXXXXXXXXXXXXX')
    // console.log(pedido)
    return (
        <Container style={{ backgroundColor: 'white' }}>
            <HeaderComprae hidelogo={true} showbackicon={true} navigation={navigation}
                // icons={[{ name: 'ios-trash', onPress: () => { callClearCarrinho() } }]}
                title='PAGAMENTO'></HeaderComprae>
            <View style={{ flex: 1, backgroundColor: 'white' }}>
                <View style={{ flex: 3, margin: 10, marginTop: 45, borderWidth: 1, borderColor: commonStyles.colors.grayBackground }}>
                    <Text style={{ marginLeft: 10, fontSize: 20, fontWeight: 'bold', marginTop: -40 }}>Endereço de entrega</Text>
                    <TouchableOpacity style={{
                        position: 'absolute', right: 5, top: 5, width: 50, height: 50, borderRadius: 25,
                        backgroundColor: commonStyles.colors.buttonGreen, justifyContent: 'center', alignItems: 'center'
                    }}
                        activeOpacity={0.7}
                        onPress={() => {
                            navigation.navigate('MeusEnderecos', {
                                onGoBack: () => {
                                    console.log('ON GO BACK')
                                    loadUser(true)
                                },
                            })
                        }}>
                        <Icon name="md-repeat" size={30}
                            color={commonStyles.colors.secondary} />
                    </TouchableOpacity>
                    <Text style={{ marginLeft: 11, marginTop: 15, fontSize: 18, marginBottom: 5 }}> {user.nome}</Text>
                    {endereco}
                    <View style={{ flexDirection: 'row', marginTop: 20 }}>
                        <CheckBox checked={retirar} color={commonStyles.colors.buttonGreen} onPress={() => setRetirar(!retirar)} />
                        <Text style={{ marginLeft: 20 }}>Retirar no Estabelecimento</Text>
                    </View>
                </View>
                <View style={{ flex: 3, backgroundColor: '#EEE', margin: 10, justifyContent: 'space-between' }}>
                    <Text style={{ marginLeft: 10, marginTop: 5, fontSize: 20, fontWeight: 'bold' }}>Resumo do Pedido</Text>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                        <Text style={{ marginLeft: 20, fontSize: 15 }}>{pedido.qtdeItens} produtos</Text>
                        <Text style={{ fontSize: 18, marginRight: 11 }}>R$ {(pedido.valorprodutos).toFixed(2).toString().replace('.', ',')}</Text>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text style={{ marginLeft: 20, fontSize: 15 }}>Entrega</Text>
                        <Text style={{ fontSize: 18, marginRight: 11 }}>R$ {(market ? parseFloat(market.freteapp) : 0.00).toFixed(2).toString().replace('.', ',')}</Text>
                    </View>
                    <View style={{
                        flexDirection: 'row', justifyContent: 'space-between', borderTopWidth: 1, marginTop: 20,
                        marginLeft: 8, marginRight: 8, marginBottom: 15, paddingTop: 10
                    }}>
                        <Text style={{ marginLeft: 11, fontSize: 20, fontWeight: 'bold' }}>Total</Text>
                        <Text style={{ marginLeft: 11, fontSize: 20, marginRight: 11, fontWeight: 'bold' }}>R$ {(pedido.valortotal).toFixed(2).toString().replace('.', ',')}</Text>
                    </View>
                </View>
                <View style={{ flex: 3, backgroundColor: 'white', margin: 10, borderColor: commonStyles.colors.grayBackground }}>
                    <Text style={{ marginLeft: 10, fontSize: 20, fontWeight: 'bold' }}>Forma de pagamento</Text>
                    <ListItem>
                        <Left>
                            <Text>Dinheiro</Text>
                        </Left>
                        <Right>
                            <Radio selected={pagamento === 'dinheiro' ? true : false} onPress={() => setPagamento('dinheiro')} />
                        </Right>
                    </ListItem>
                    <ListItem>
                        <Left>
                            <Text>Cartão</Text>
                        </Left>
                        <Right>
                            <Radio selected={pagamento === 'cartao' ? true : false} onPress={() => setPagamento('cartao')} />
                        </Right>
                    </ListItem>
                </View>
            </View>
            <Wait visible={showWait}></Wait>
            <ShowMessage1 visible={showMessagePurchase}></ShowMessage1>
            {pedido != null && pedido.qtdeItens > 0 ?
                <Button style={{ marginBottom: 5, marginLeft: 5, marginRight: 5, justifyContent: 'center', backgroundColor: commonStyles.colors.buttonGreen }}
                    onPress={() => { sendPurchaseOrder() }}>
                    <Text>Finalizar Pedido</Text>
                </Button> : <Text></Text>}
            <FooterTabs navigation={navigation}></FooterTabs>
        </Container>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    background: {
        flex: 3
    },
    taskList: {
        flex: 7
    },
    titleBar: {
        flex: 1,
        justifyContent: 'flex-end'
    },
    title: {
        fontFamily: commonStyles.fontFamily,
        color: commonStyles.colors.secondary,
        fontSize: 35,
        marginLeft: 20,
        marginBottom: 20
    },
    subtitle: {
        fontFamily: commonStyles.fontFamily,
        color: commonStyles.colors.secondary,
        fontSize: 20,
        marginLeft: 20,
        textAlign: 'center',
        fontWeight: 'bold'
    },
    iconBar: {
        flexDirection: 'row',
        marginHorizontal: 20,
        justifyContent: 'flex-end',
        marginTop: Platform.OS === 'ios' ? 40 : 10
    },
    addButton: {
        position: 'absolute',
        right: 30,
        top: 90,
        width: 50,
        height: 50,
        borderRadius: 25,
        backgroundColor: commonStyles.colors.today,
        justifyContent: 'center',
        alignItems: 'center'
    },
    CardInfoBody: {
        flex: 3,
        backgroundColor: '#EEE',
        margin: 10
    },
    CardInfoTitle: {
        marginLeft: 10,
        fontSize: 25,
        fontWeight: 'bold'
    },
});
FinalizarCompra.navigationOptions = {
    title: 'Car',
    headerShown: false
}
export default FinalizarCompra