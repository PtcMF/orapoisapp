import React, { useState, useEffect } from 'react'
import { StyleSheet, ImageBackground, Modal, TouchableWithoutFeedback } from 'react-native'
import { Container, Button, Text, View } from 'native-base';
import FooterTabs from '../components/FooterTabs';
import * as Font from 'expo-font';
import commonStyles from '../commonStyles'
import { AsyncStorage } from 'react-native';
import { getLoggedUser } from '../controllers/Loginctrl'
import Icon from 'react-native-vector-icons/Ionicons'
import Loading from '../components/Loading';
import ProductCard from '../components/ProductCard';
import { FlatList } from 'react-native-gesture-handler';
import HeaderComprae from '../components/HeaderComprae';
const DetalhesCompra = ({ navigation }) => {
    const [fontLoaded, setFontLoaded] = useState(false);
    const [pedido, setPedido] = useState(null);
    const [produtos, setProdutos] = useState([]);
    const [user, setUser] = useState(null);
    const [modalVisible, setModalVisible] = useState(false);
    const [showWait, setShowWait] = useState(false);
    let carregarFonts = async () => {
        await Font.loadAsync({
            Roboto: require("native-base/Fonts/Roboto.ttf"),
            Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
            Lato: require("../../assets/fonts/Lato.ttf")
        });
        setFontLoaded(true);
    }
    useEffect(() => {
        carregarFonts();
    }, []);
    console.log('DETALHE XXXXXXXXX')
    console.log(navigation.state.params['pedido'])
    if (!fontLoaded) {
        return <Loading></Loading>
    }
    let loadUser = async (force) => {
        if (user === null || force === true) {
            let usr = await getLoggedUser()
            setUser(usr)
        }
    }
    loadUser()
    if (navigation.state.params['pedido']) {
        let tmpPed = JSON.stringify(navigation.state.params['pedido'])
        setPedido(JSON.parse(tmpPed))
        navigation.state.params['pedido'] = null
    } else if (pedido == null) {
        return <Loading></Loading>
    }

    //CARREGAR LISTA DE PRODUTOS DO CARRINHO
    let valorTotalCompra = 0.0

    if (pedido === null) {

        return <Loading></Loading>
    }
    console.log(pedido.cliente)
    console.log(pedido.cliente.pessoa)
    let endereco = (<View>
        <Text style={{ marginLeft: 15, fontSize: 15 }}>{pedido.cliente.pessoa.endereco.endereco}, {pedido.cliente.pessoa.endereco.numero}, {pedido.cliente.pessoa.endereco.referencia} </Text>
        <Text style={{ marginLeft: 15, fontSize: 15 }}>{pedido.cliente.pessoa.endereco.bairro}</Text>
        {/* <Text style={{ marginLeft: 15, fontSize: 15 }}>{user.selectedAddress.city.cidade} - {user.selectedAddress.uf.siglauf}</Text> */}
        {/* <Text style={{ marginLeft: 15, fontSize: 15 }}>CEP: 85605-270</Text> */}
    </View>);
    onCancel = () => {
        console.log('CANCEL')
        setModalVisible(false)
    }
    let modalProdutos = (<Modal transparent={true} visible={modalVisible} animationType='slide'>
        <TouchableWithoutFeedback onPress={onCancel}>
            <View style={{ flex: 1, backgroundColor: 'rgba(0, 0, 0, 0.7)' }}></View>
        </TouchableWithoutFeedback>
        <View style={{ backgroundColor: '#FFF', flex:8 }}>
            <FlatList data={produtos} keyExtractor={item => item.idfilialitem}
                renderItem={({ item }) => {
                    return (<ProductCard {...item}></ProductCard>);
                }}
            />
        </View>
        <TouchableWithoutFeedback
            onPress={onCancel}>
            <View style={{ flex: 1, backgroundColor: 'rgba(0, 0, 0, 0.7)' }}></View>
        </TouchableWithoutFeedback>
    </Modal>)
    console.log('XXXXXXXXXXXXXXXXXX PEDIDO XXXXXXXXXXXXXXXXXXX')
    console.log(pedido)
    return (
        <Container style={{ backgroundColor: commonStyles.colors.grayBackground }}>
            {modalProdutos}
            <View style={styles.container}>
                <HeaderComprae hidelogo={true} showbackicon={true} navigation={navigation}
                    title='MEU PEDIDO'
                ></HeaderComprae>
                <View style={styles.CardInfoBody}>
                    <Text style={styles.CardInfoTitle}>Endereço</Text>
                    <Text style={{ marginLeft: 11, marginTop:5, fontSize: 18 }}> {pedido.cliente.pessoa.nome}</Text>
                    {endereco}
                </View>
                <View style={styles.CardInfoBody}>
                    <Text style={styles.CardInfoTitle}>Resumo do Pedido</Text>
                    <View style={{ justifyContent: 'space-between', flex: 9 }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
                            <Text style={{ marginLeft: 11, fontSize: 15 }}>{pedido.qtdeItens} produtos</Text>
                            <Text style={{ marginLeft: 11, fontSize: 18, marginRight: 11 }}>R$ {(pedido.valorprodutos) ? (pedido.valorprodutos).toFixed(2).toString().replace('.', ',') : '0.00'}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <Text style={{ marginLeft: 11, fontSize: 15 }}>Entrega</Text>
                            <Text style={{ marginLeft: 11, fontSize: 18, marginRight: 11 }}>R$ {(pedido.frete) ? (frete).toFixed(2).toString().replace('.', ',') : '0,00'}</Text>
                        </View>
                        <View style={{
                            flexDirection: 'row', justifyContent: 'space-between', borderTopWidth: 1, marginTop: 20,
                            marginLeft: 8, marginRight: 8
                        }}>
                            <Text style={{ marginLeft: 11, fontSize: 20, fontWeight: 'bold' }}>Total</Text>
                            <Text style={{ marginLeft: 11, fontSize: 20, marginRight: 11, fontWeight: 'bold' }}>R$ {(pedido.valortotal) ? (pedido.valortotal).toFixed(2).toString().replace('.', ',') : '0,00'}</Text>
                        </View>
                        <Button style={{ justifyContent: 'center', backgroundColor: commonStyles.colors.buttonGreen, borderRadius: 5, marginTop: 5, marginLeft:10, marginRight: 10, marginBottom:5 }}
                            onPressOut={() => {
                                setModalVisible(true)
                                setProdutos(pedido.produtos)
                            }}>
                            <Text style={{ textAlign: 'center' }}>DETALHES</Text>
                        </Button>
                    </View>

                </View>
                <View style={styles.CardInfoBody}>
                    <Text style={styles.CardInfoTitle}>Pagamento</Text>
                    <Text style={styles.CardInfoTitle}>{pedido.pagamento}</Text>

                </View>
            </View>
            <FooterTabs navigation={navigation}></FooterTabs>
        </Container>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    background: {
        flex: 3
    },
    taskList: {
        flex: 7
    },
    titleBar: {
        flex: 1,
        justifyContent: 'flex-end'
    },
    title: {
        fontFamily: commonStyles.fontFamily,
        color: commonStyles.colors.secondary,
        fontSize: 35,
        marginLeft: 20,
        marginBottom: 20
    },
    subtitle: {
        fontFamily: commonStyles.fontFamily,
        color: commonStyles.colors.secondary,
        fontSize: 20,
        marginLeft: 20,
        textAlign: 'center',
        fontWeight: 'bold'
    },
    iconBar: {
        flexDirection: 'row',
        marginHorizontal: 20,
        justifyContent: 'flex-end',
        marginTop: Platform.OS === 'ios' ? 40 : 10
    },
    addButton: {
        position: 'absolute',
        right: 30,
        top: 90,
        width: 50,
        height: 50,
        borderRadius: 25,
        backgroundColor: commonStyles.colors.today,
        justifyContent: 'center',
        alignItems: 'center'
    },
    CardInfoBody: {
        flex: 3,
        backgroundColor: 'white',
        margin: 10,
    },
    CardInfoTitle: {
        marginLeft: 10,
        fontSize: 25,
        fontWeight: 'bold'
    },
});
DetalhesCompra.navigationOptions = {
    title: 'DetalhesCompra',
    headerShown: false
}
export default DetalhesCompra