import React, { useState, useEffect } from 'react'
import { StyleSheet, ImageBackground, Image, FlatList, TouchableOpacity } from 'react-native'
import { Container, Header, Content, Item, Input, Icon, Button, Text, View } from 'native-base';
import { AsyncStorage } from 'react-native';
import * as Font from 'expo-font';
import commonStyles from '../commonStyles'
import ProductCard from '../components/ProductCard';
import FooterTabs from '../components/FooterTabs';
import Loading from '../components/Loading';
import HeaderComprae from '../components/HeaderComprae';
import { getLoggedUser } from '../controllers/Loginctrl'
import { clearCarrinho } from '../controllers/Produtosctrl';
import { getSelectedMarket } from '../controllers/Mercadoctrl';
const CarrinhoCompras = ({ navigation }) => {
    //PRODUTOS
    const [fontLoaded, setFontLoaded] = useState(false);
    const [produtos, setProdutos] = useState(null);
    const [total, setTotal] = useState(0);
    const [frete, setFrete] = useState(5);
    const [market, setMarket] = useState(null);
    let carregarFonts = async () => {
        await Font.loadAsync({
            Lato: require("../../assets/fonts/Lato.ttf"),
            Segoe: require("../../assets/fonts/segoe-ui.otf"),
            SegoeBold: require("../../assets/fonts/segoe-ui-bold.otf")
        });
        setFontLoaded(true);
    }
    useEffect(() => {
        carregarFonts();
    }, []);
    console.log('CARRINHO XXXXXXXXX')
    console.log(navigation)

    if (!fontLoaded) {
        return <Loading></Loading>
    }
    if (navigation.state.params['refresh'] === true) {
        navigation.state.params['refresh'] = false
        if (produtos !== null) {
            setProdutos(null)
        }
    }

    //FUNCAO PARA ATUALIZAR TOTAL
    let updateQuantidade = (idfilialitem, qtde) => {
        valorTotalCompra = 0.0
        try {
            AsyncStorage.getItem('carrinho', (err, result) => {
                let carrinho = {};
                if (result) {
                    carrinho = JSON.parse(result);
                }
                let item = carrinho[idfilialitem];
                item["quantidade"] = qtde
                AsyncStorage.setItem('carrinho', JSON.stringify(carrinho));
                carregarProdutos();
            })

        } catch (error) {
            console.log('ERROR')
            console.log(error)
        }
    }
    //FUNCAO PARA DELETAR ITEM
    let onDelete = (idfilialitem) => {
        valorTotalCompra = 0.0
        try {
            AsyncStorage.getItem('carrinho', (err, result) => {
                let carrinho = {};
                if (result) {
                    carrinho = JSON.parse(result);
                }
                delete carrinho[idfilialitem];
                AsyncStorage.setItem('carrinho', JSON.stringify(carrinho));
                carregarProdutos();
            })

        } catch (error) {
            console.log('ERROR')
            console.log(error)
        }
    }
    //FECHAR CARRINHO DE COMPRAS
    let fecharCarrinho = async () => {
        let user = await getLoggedUser();
        if (user != null && user.email != null && user.nome != null) {
            navigation.navigate('FinalizarCompra', {
                refresh: () => {
                    carregarProdutos()
                },
            })
        } else if (user != null && user.email != null) {
            navigation.navigate('Profile')
        } else {
            navigation.navigate('Register')
        }
    }
    //CARREGAR LISTA DE PRODUTOS DO CARRINHO
    let valorTotalCompra = 0.0
    let carregarProdutos = async () => {
        try {
            await AsyncStorage.getItem('carrinho', (err, result) => {
                let carrinho = {};
                if (result) {
                    carrinho = JSON.parse(result);
                }
                let prods = [];
                for (var [key, value] of Object.entries(carrinho)) {
                    value["onDelete"] = onDelete;
                    value["updateQuantidade"] = updateQuantidade;
                    value["editar"] = true;
                    let totalProdutos = value.quantidade * value.precovarejo
                    valorTotalCompra = valorTotalCompra + totalProdutos
                    prods.push(value)
                }
                setProdutos(prods)
                setTotal(valorTotalCompra)
            })
        } catch (error) {
            console.log('ERROR')
            console.log(error)
        }

    }

    let callClearCarrinho = () => {
        clearCarrinho();
        setProdutos(null)
    }
    let jsxProdutos = (<View style={styles.taskList, { flex: 9 }}>
        <FlatList data={produtos}
            keyExtractor={item => `${item.idfilialitem}`}
            renderItem={({ item }) => {
                return (
                    <ProductCard {...item} ></ProductCard>
                    // <View></View>
                );
            }}>
        </FlatList>
    </View>)
    if (produtos == null) {
        jsxProdutos = (<Loading></Loading>)
        carregarProdutos()
    }
    carregarMercado = async () => {
        let _market = await getSelectedMarket();
        if (_market) {
            setMarket(_market)
        }
    }
    if (market == null) {
        jsxProdutos = (<Loading></Loading>)
        carregarMercado()
    }
    return (
        <Container style={{ backgroundColor: commonStyles.colors.grayBackground }}>

            <View style={styles.container}>
                <HeaderComprae hidelogo={true} showbackicon={true} navigation={navigation}
                    icons={[{ name: 'ios-trash', onPress: () => { callClearCarrinho() } }]}
                    title='CARRINHO DE COMPRAS'
                ></HeaderComprae>
                {jsxProdutos}
            </View>
            <View style={{ height: 150, justifyContent: 'space-between', backgroundColor: 'white' }}>
                <View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginLeft: 15, marginRight: 15, marginTop: 10 }}>
                        <Text style={{ fontFamily: commonStyles.fontFamily }}>ENTREGA</Text>
                        <Text style={{ fontFamily: commonStyles.fontFamily }}>R$ {market?market.freteapp : 0.00 }</Text>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginLeft: 15, marginRight: 15, marginTop: 15 }}>
                        <Text style={{ fontFamily: commonStyles.fontFamily, textAlignVertical: 'bottom' }}>TOTAL DO PEDIDO</Text>
                        <Text style={{ fontFamily: commonStyles.fontFamilyBold, fontSize: 22 }}>R$ {(total + (market? parseFloat(market.freteapp) : 0.00)).toFixed(2).replace('.', ',')}</Text>
                    </View>
                </View>
                {(produtos != null && produtos.length > 0) ? (
                    <Button style={{
                        marginBottom: 10, marginLeft: 15, marginRight: 15, justifyContent: 'center',
                        backgroundColor: commonStyles.colors.buttonGreen
                    }}
                        onPress={() => {
                            fecharCarrinho();
                        }}>
                        <Text>Fechar Carrinho</Text>
                    </Button>)
                    : <View></View>}
            </View>
            <FooterTabs navigation={navigation}></FooterTabs>
        </Container>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    background: {
        flex: 3
    },
    taskList: {
        flex: 7
    },
    titleBar: {
        flex: 1,
        justifyContent: 'flex-end'
    },
    title: {
        fontFamily: commonStyles.fontFamily,
        color: commonStyles.colors.secondary,
        fontSize: 35,
        marginLeft: 20,
        marginBottom: 20
    },
    subtitle: {
        fontFamily: commonStyles.fontFamily,
        color: commonStyles.colors.secondary,
        fontSize: 20,
        marginLeft: 20,
        textAlign: 'center',
        fontWeight: 'bold'
    },
    iconBar: {
        flexDirection: 'row',
        marginHorizontal: 20,
        justifyContent: 'flex-end',
        marginTop: Platform.OS === 'ios' ? 40 : 10
    },
    addButton: {
        position: 'absolute',
        right: 30,
        top: 90,
        width: 50,
        height: 50,
        borderRadius: 25,
        backgroundColor: commonStyles.colors.today,
        justifyContent: 'center',
        alignItems: 'center'
    }
});
CarrinhoCompras.navigationOptions = {
    title: 'Car',
    headerShown: false
}
export default CarrinhoCompras