import React, { useState, useEffect } from 'react'
import { StyleSheet, ImageBackground, Image, FlatList, TouchableOpacity } from 'react-native'
import { Container, Icon, Button, Text, View } from 'native-base';
import Swipeable from 'react-native-gesture-handler/Swipeable'
import FooterTabs from '../components/FooterTabs';
import * as Font from 'expo-font';
import commonStyles from '../commonStyles'
import Loading from '../components/Loading';
import { getLoggedUser } from '../controllers/Loginctrl'
import { callEditAddress, callDeleteAddress, callSelectedAddress } from '../controllers/Enderecoctrl'
import HeaderComprae from '../components/HeaderComprae';
import imgLambreta from '../../assets/imgs/lambreta.jpeg'

const MeusEnderecos = ({ navigation }) => {
    console.log('ENDERECOS XXXXXXXXX')
    // console.log(props)
    const [fontLoaded, setFontLoaded] = useState(false);
    const [user, setUser] = useState(null);
    const [addresses, setAddresses] = useState(null);
    let carregarFonts = async () => {
        await Font.loadAsync({
            Roboto: require("native-base/Fonts/Roboto.ttf"),
            Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
            Lato: require("../../assets/fonts/Lato.ttf")
        });
        setFontLoaded(true);
    }
    useEffect(() => {
        carregarFonts();
    }, []);
    if (!fontLoaded) {
        return <Loading></Loading>
    }

    let loadUser = async (force) => {
        if (user === null || force) {
            let usr = await getLoggedUser()
            setUser(usr)
        }
    }
    loadUser()
    //CARREGAR LISTA DE ENDERECOS

    let loadAdressess = () => {
        if (user === null) {
            return
        }
        if (addresses) {
            return
        }
        if (user.addresses) {
            setAddresses(user.addresses)
        } else {
            setAddresses([])
        }
    }
    loadAdressess()
    refresh = () => {
        console.log('REFRESH MEUS ENDERECOS')
        setTimeout(() => {
            console.log('REFRESH MEUS ENDERECOS 66666')
            setUser(null)
            setAddresses(null)
        }, 200)

    }
    newAddress = () => {
        callEditAddress({}, navigation, refresh)
    }
    editAddress = (address) => {
        callEditAddress(address.item, navigation, refresh)
    }
    deleteAddress = async (address) => {
        await callDeleteAddress(address.item, user)
        refresh()
    }
    const GetRightContent = ({ progress, item }) => {
        return (
            <View>
                <TouchableOpacity style={{
                    flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end',
                    paddingHorizontal: 20, flex: 5, borderBottomWidth: 1, borderBottomColor: commonStyles.colors.buttonGreen
                }}
                    onPress={() => { editAddress({ item }) }}>
                    <Icon name="md-create" size={30} style={{ color: commonStyles.colors.buttonGreen }} />
                </TouchableOpacity>
                <TouchableOpacity style={{
                    flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end',
                    paddingHorizontal: 20, flex: 5, borderBottomWidth: 1, borderBottomColor: commonStyles.colors.buttonGreen
                }}
                    onPress={() => { deleteAddress({ item }) }}>
                    <Icon name="ios-trash" size={30} style={{ color: commonStyles.colors.buttonGreen }} />
                </TouchableOpacity>
            </View>
        )
    }
    let jsxEnderecos = (<FlatList data={addresses}
        keyExtractor={item => `${item.numero}`}
        renderItem={({ item }) => {
            return (
                <Swipeable renderRightActions={(progress, dragX) => (
                    <GetRightContent progress={progress} dragX={dragX} item={item} />
                )}>
                    <View style={{
                        flexDirection: 'row', borderColor: '#AAA', borderBottomWidth: 1,
                        paddingVertical: 10, backgroundColor: '#FFF'
                    }}>
                        <Image
                            source={imgLambreta}
                            style={{ marginLeft: 10, width: 60, height: 60, borderRadius: 75 }}
                        />
                        <View style={{ flex: 6, marginRight: 15, marginLeft: 10, top: 2, borderColor: 'black', borderWidth: 0, justifyContent: 'space-between' }}>
                            <Text style={{ marginLeft: 15, fontSize: 13 }}>{item.endereco}, {item.numero}, {item.referencia} </Text>
                            <Text style={{ marginLeft: 15, fontSize: 13 }}>{item.bairro}</Text>
                            <Text style={{ marginLeft: 15, fontSize: 13 }}>{item.city.cidade} - {item.uf.siglauf}</Text>
                        </View>
                        <TouchableOpacity style={{
                            position: 'absolute', right: 15, alignSelf: 'center', width: 50, height: 50, borderRadius: 25,
                            backgroundColor: commonStyles.colors.buttonGreen, justifyContent: 'center', alignItems: 'center'
                        }}
                            activeOpacity={0.7}
                            onPress={async () => {
                                await callSelectedAddress(item)
                                navigation.goBack()
                                if (navigation.state.params['onGoBack']) {
                                    navigation.state.params.onGoBack()
                                }
                            }}>
                            <Icon name="ios-checkmark" size={30}
                                color={commonStyles.colors.secondary} />
                        </TouchableOpacity>
                    </View>
                </Swipeable>
            );
        }}>
    </FlatList>)
    if (addresses && addresses.length <= 0) {
        jsxEnderecos = (<View style={{ flexDirection: 'column', alignSelf: 'center', flex: 10 }}>
            <View style={{ flex: 10 }}></View>
            <View style={{ flex: 1, flexDirection: 'row' }}>
                <Icon name='alert'></Icon>
                <Text style={{ marginLeft: 10 }}>Você não cadastrou nenhum endereço!</Text>
            </View>
            <View style={{ flex: 10 }}></View>
        </View>)
    }
    return (
        <Container>
            <View style={styles.container}>
                <HeaderComprae hidelogo></HeaderComprae>
                <View style={styles.taskList, { flex: 9 }}>
                {jsxEnderecos}
                </View>
            </View>
            <Button style={{ marginBottom: 5, marginLeft: 5, marginRight: 5, justifyContent: 'center', backgroundColor: commonStyles.colors.buttonGreen }}
                onPress={() => {
                    newAddress();
                }}>
                <Text>Adicionar Endereço</Text>
            </Button>
        </Container >
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    background: {
        flex: 3
    },
    taskList: {
        flex: 7
    },
    titleBar: {
        flex: 1,
        justifyContent: 'flex-end'
    },
    title: {
        fontFamily: commonStyles.fontFamily,
        color: commonStyles.colors.secondary,
        fontSize: 35,
        marginLeft: 20,
        marginBottom: 20
    },
    subtitle: {
        fontFamily: commonStyles.fontFamily,
        color: commonStyles.colors.secondary,
        fontSize: 20,
        marginLeft: 20,
        textAlign: 'center',
        fontWeight: 'bold'
    },
    iconBar: {
        flexDirection: 'row',
        marginHorizontal: 20,
        justifyContent: 'flex-end',
        marginTop: Platform.OS === 'ios' ? 40 : 10
    },
    addButton: {
        position: 'absolute',
        right: 30,
        top: 90,
        width: 50,
        height: 50,
        borderRadius: 25,
        backgroundColor: commonStyles.colors.today,
        justifyContent: 'center',
        alignItems: 'center'
    }
});
MeusEnderecos.navigationOptions = {
    title: 'MeusEnderecos',
    headerShown: false
}
export default MeusEnderecos