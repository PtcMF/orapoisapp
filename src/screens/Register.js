import React, { useState, useEffect } from 'react'
import { Image, ImageBackground } from 'react-native';
import { Container, Header, Text, View, Item, Input, Icon, Button, Toast } from 'native-base';
import * as Font from 'expo-font';
import { verificarCredenciais, confirmEmail, getLoggedUser, logout } from '../controllers/Loginctrl'
import validate from 'validate.js'
import PV from '../components/PraeValidation'
import Loading from '../components/Loading';
import { Keyboard } from 'react-native'
import commonStyles from '../commonStyles';
import HeaderComprae from '../components/HeaderComprae';
import { showMessage } from '../components/Utils';
import logoComprae from '../../assets/imgs/logo_comprae.png'
const Register = ({ navigation }) => {
    const [email, setEmail] = useState('');
    const [fontLoaded, setFontLoaded] = useState(false);
    const [confirmar, setConfirmar] = useState(false);
    const [showLoading, setShowLoading] = useState(false);
    let jsxBody
    //EVENTOS
    let carregarFonts = async () => {
        await Font.loadAsync({
            Roboto: require("native-base/Fonts/Roboto.ttf"),
            Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
            Lato: require("../../assets/fonts/Lato.ttf")
        });
        setFontLoaded(true);
    }
    useEffect(() => {
        carregarFonts();
    }, []);
    handleEmailChange = (emailvalue) => {
        setEmail(emailvalue);
    };
    getUser = async () => {
        let user = await getLoggedUser();
        console.log('USER 123')
        console.log(user)
        if (user) {
            if (user.email) {
                if (user.nome) {
                    navigation.navigate('Products')
                } else {
                    navigation.navigate('Profile')
                }
            }
        }
    }
    getUser();
    validateEmail = () => {
        let valResult = validate({ 'email': email }, PV)
        if (valResult['email']) {
            console.log(valResult["email"][0])
            showMessage(valResult["email"][0])
            return false
        }
        return true
    }
    solicitarCodigoEmail = async () => {
        if (validateEmail()) {
            console.log('enviar confirmacao email')
            setShowLoading(true)
            await logout()
            let enviado = await verificarCredenciais(email, navigation);
            setConfirmar(enviado)
            setShowLoading(false)
            if (enviado === false) {
                alert('Não foi possível enviar o código de confirmação')
            }
        }
    };
    confirmCodigoEmail = (codigo) => {
        confirmEmail(codigo, email, navigation)
    };
    console.log('Register XXXXXXXXX')

    if (fontLoaded === false) {
        return (<Loading></Loading>)
    }
    _telaConfirmarCodigo = () => {
        let firstTextInput, secondTextInput, thirdTextInput, fourthTextInput, btnConfirm;
        let a, b, c, d;
        return (<Container>
            <HeaderComprae hidelogo showbackicon title='CONFIRMAR CÓDIGO' navigation={navigation}></HeaderComprae>

            <View style={{ flex: 9, justifyContent: 'center', backgroundColor:commonStyles.colors.grayBackground }}>
                <View style={{ flexDirection: 'row', flex: 3, justifyContent: 'center' }}>
                    <View style={{ justifyContent: 'center' }}>
                        <Image
                            source={logoComprae}
                            style={{ width: 120, height: 60, borderRadius: 0 }}
                        />
                    </View>
                </View>
                <View style={{ flex: 5, padding: 40 }}>
                    <Text style={{ color: 'black', textAlign: 'center', marginBottom: 20 }}>CÓDIGO DE CONFIRMAÇÃO</Text>
                    <View style={{ justifyContent: 'space-between', flexDirection: 'row' }}>
                        <Item color="black" style={{ width: 50 }}>
                            <Input style={{ color: 'black', textAlign: 'center', fontSize: 35 }} maxLength={1}
                                placeholderTextColor='black' autoFocus={true}
                                keyboardType='decimal-pad'
                                ref={(input) => { firstTextInput = input }}
                                onChangeText={(char) => {
                                    if (char.length > 0) {
                                        a = char
                                        secondTextInput._root.focus()
                                    }
                                }} />
                        </Item>
                        <Item color="black" style={{ width: 50 }}>
                            <Input style={{ color: 'black', textAlign: 'center', fontSize: 35 }} maxLength={1}
                                placeholderTextColor='black' autoFocus={false}
                                keyboardType='decimal-pad'
                                ref='secondTextInput'
                                ref={(input) => { secondTextInput = input }}
                                onChangeText={(char) => {
                                    if (char.length > 0) {
                                        b = char
                                        thirdTextInput._root.focus()
                                    }
                                }} />
                        </Item>
                        <Item color="white" style={{ width: 50 }}>
                            <Input style={{ color: 'black', textAlign: 'center', fontSize: 35 }} maxLength={1}
                                placeholderTextColor='black' autoFocus={false}
                                keyboardType='decimal-pad'
                                ref='secondTextInput'
                                ref={(input) => { thirdTextInput = input }}
                                onChangeText={(char) => {
                                    if (char.length > 0) {
                                        c = char
                                        fourthTextInput._root.focus()
                                    }
                                }} />
                        </Item>
                        <Item color="white" style={{ width: 50 }}>
                            <Input style={{ color: 'black', textAlign: 'center', fontSize: 35 }} maxLength={1}
                                placeholderTextColor='black' autoFocus={false}
                                keyboardType='decimal-pad'
                                ref='secondTextInput'
                                ref={(input) => { fourthTextInput = input }}
                                onChangeText={(char) => {
                                    if (char.length > 0) {
                                        d = char
                                        Keyboard.dismiss()
                                        console.log('======================')
                                        let codigo = a + b + c + d
                                        console.log(codigo)
                                        confirmCodigoEmail(codigo);
                                    }
                                }} />
                        </Item>

                    </View>
                    <View style={{ justifyContent: 'center', marginTop: 70 }}>
                        <Button style={{ justifyContent: 'center', backgroundColor: commonStyles.colors.buttonGreen, borderRadius: 5 }} onPress={() => {
                            let codigo = a + b + c + d
                            console.log(codigo)
                            confirmCodigoEmail(codigo);
                        }} >
                            <Text style={{ textAlign: 'center' }}>CONFIRMAR</Text>
                        </Button>
                    </View>
                </View>
            </View>
        </Container>);
    }

    jsxBody = (<Loading></Loading>)
    if (confirmar) {
        return _telaConfirmarCodigo();
    }

    if (!showLoading) {
        jsxBody = (<View style={{ flex: 9, justifyContent: 'center', backgroundColor: commonStyles.colors.grayBackground }}>
            <View style={{ flexDirection: 'row', flex: 3, justifyContent: 'center' }}>
                <View style={{ justifyContent: 'center' }}>
                    <Image
                        source={logoComprae}
                        style={{ width: 120, height: 60, borderRadius: 0 }}
                    />
                </View>
            </View>
            <View style={{ flex: 5, padding: 40 }}>
                <View style={{ justifyContent: 'space-between' }}>
                    <Text style={{ textAlign: 'center' }}>Informe seu e-mail</Text>
                    <Item>
                        <Icon name='ios-mail' style={{ color: 'black' }} />
                        <Input style={{ color: 'black' }} placeholderTextColor='black' autoCapitalize='none'
                            onChangeText={(text) => handleEmailChange(text)} />
                    </Item>
                </View>
                <View style={{ justifyContent: 'center', marginTop: 70 }}>
                    <Button style={{ justifyContent: 'center', backgroundColor: commonStyles.colors.buttonGreen, borderRadius: 5 }}
                        onPressOut={() => solicitarCodigoEmail()}>
                        <Text style={{ textAlign: 'center' }}>ENVIAR CÓDIGO</Text>
                    </Button>
                </View>
            </View>
        </View>)
    }
    return (
        <Container>
            <HeaderComprae hidelogo showbackicon navigation={navigation} title='CRIAR CONTA'></HeaderComprae>
            {jsxBody}
        </Container>
    );
}
Register.navigationOptions = {
    title: 'Register',
    headerShown: false
}

export default Register