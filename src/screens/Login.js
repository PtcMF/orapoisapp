import React, { useState, useEffect } from 'react'
import { Image, ImageBackground } from 'react-native';
import { Container, Header, Text, View, Item, Input, Icon, Button } from 'native-base';
import * as Font from 'expo-font';
import loginFundoImg from '../../assets/imgs/logo_praesoft.png'
import { verificarCredenciais } from '../controllers/Loginctrl'
const SignIn = ({ navigation }) => {
    //EVENTOS
    handleEmailChange = (email) => {
        this.setState({ email });
    };

    handlePasswordChange = (password) => {
        this.setState({ password });
    };

    handleCreateAccountPress = () => {
        this.props.navigation.navigate('SignUp');
    };
    handleSignInPress = async () => {
    };
    verificarCred = () => {
        loginOK();
        if (verificarCredenciais() === true) {
            return;
        }
        // alert('Credenciais não encontradas')
    }
    loginOK = () => {
        navigation.navigate('Products');
    };
    const [fontLoaded, setFontLoaded] = useState(false);
    let carregarFonts = async () => {
        await Font.loadAsync({
            Roboto: require("native-base/Fonts/Roboto.ttf"),
            Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
            Lato: require("../../assets/fonts/Lato.ttf")
        });
        setFontLoaded(true);
    }
    useEffect(() => {
        carregarFonts();
    }, []);
    console.log('PRODUTOS XXXXXXXXX')
    if (!fontLoaded) {
        return <Text>aaaa</Text>
    }
    console.log(loginFundoImg)
    return (
        <Container>
            <Header style={{ height: 35 }}>
            </Header>
            <ImageBackground source={loginFundoImg} style={{ flex: 9, opacity: .9 }}>
                <View style={{ flex: 9, justifyContent: 'center' }}>
                    <View style={{ flexDirection: 'row', flex: 3, justifyContent: 'center' }}>
                        <View style={{ justifyContent: 'center' }}>
                            <Image
                                source={{ uri: 'https://www.supermercadobrasao.com.br/images/site/logo-brasao-guaratuba.png' }}
                                style={{ width: 120, height: 60, borderRadius: 15 }}
                            />
                        </View>
                    </View>
                    <View style={{ flex: 5, padding: 40 }}>
                        <View style={{ justifyContent: 'space-between' }}>
                            <Item color="white">
                                <Icon name='ios-person' style={{ color: '#FFF' }} />
                                <Input style={{ color: '#FFF' }} placeholderTextColor='#FFF' placeholder='Login' />
                            </Item>
                            <Item style={{ marginTop: 40 }}>
                                <Icon active name='md-lock' style={{ color: '#FFF' }} />
                                <Input style={{ color: '#FFF' }} placeholderTextColor='#FFF' placeholder='Password' />
                            </Item>
                        </View>
                        <View style={{ justifyContent: 'center', marginTop: 70 }}>
                            <Button style={{ justifyContent: 'center', backgroundColor: 'blue', borderRadius: 25 }}
                                onPressOut={() => verificarCred()}>
                                <Text style={{ textAlign: 'center' }}>Login</Text>
                            </Button>
                            <Text style={{ textAlign: 'center', marginTop: 20, color: 'white' }}
                            onPress={() => alert('registrar')}>Register Here</Text>
                        </View>
                    </View>
                </View>
            </ImageBackground>
        </Container>
    );
}
SignIn.navigationOptions = {
    title: 'SignIn',
    headerShown: false
}
export default SignIn