import React, { Component } from 'react'
import {
    View,
    // Text,
    StyleSheet,
    FlatList,
    Platform,
    Alert,
    Image
} from 'react-native'
import { buscarProdutos } from '../controllers/Produtosctrl'
// import Icon from 'react-native-vector-icons/FontAwesome'
import 'moment/locale/pt-br'
import commonStyles from '../commonStyles'
import ProductCard from '../components/ProductCard'
import { Container, Header, Item, Input, Icon, Button, Text } from 'native-base';
import * as Font from 'expo-font';

const initialState = {
    showDoneTasks: true,
    showAddTask: false,
    produtosBuscados: [],
    tasks: [],
    fontLoaded: false,
    filter: ''
}

export default class TaskList extends Component {
    state = {
        ...initialState
    }
    toggleFilter = () => {
        this.setState({ showDoneTasks: !this.state.showDoneTasks }, this.filterTasks)
    }

    filterTasks = () => {
        let produtosBuscados = null
        if (this.state.showDoneTasks) {
            produtosBuscados = [...this.state.tasks]
        } else {
            const pending = task => task.doneAt === null
            produtosBuscados = this.state.tasks.filter(pending)
        }

        this.setState({ produtosBuscados })
        // AsyncStorage.setItem('tasksState', JSON.stringify(this.state))
    }

    buscarProdutos1 = async (filtro) => {
        console.log('teste teste');
        this.produtosBuscados = await buscarProdutos(filtro);
        console.log(this.produtosBuscados);
        this.setState({ produtosBuscados: this.produtosBuscados })
    }
    render() {
        if (!this.state.fontLoaded) {            
            return (
                <Text>aaa</Text>
            );
          }
        return (
            <View style={styles.container}>
                
                    {/* <Header searchBar rounded>
                        <Item>
                            <Icon name="ios-search" />
                            <Input 
                            // onSubmit={(filtro) => this.setState({produtosBuscados: buscarProdutos(filtro)})} 
                            onChangeText={text => this.setState({filter: text})}
                            onBlur={ (e) =>  this.buscarProdutos1(this.state.filter)}
                            placeholder="Search" />                            
                            <Icon name="ios-checkmark-circle-outline" />
                        </Item>
                        <Button transparent>
                            <Text>Search</Text>
                        </Button>
                    </Header> */}
                
                <View style={styles.taskList}>
                    <FlatList data={this.state.produtosBuscados}
                        keyExtractor={item => `${item.idfilialitem}`}
                        renderItem={({ item }) => {
                            return (
                                <ProductCard {...item}></ProductCard>
                            );
                        }}>
                    </FlatList>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    background: {
        flex: 3
    },
    taskList: {
        flex: 7
    },
    titleBar: {
        flex: 1,
        justifyContent: 'flex-end'
    },
    title: {
        fontFamily: commonStyles.fontFamily,
        color: commonStyles.colors.secondary,
        fontSize: 35,
        marginLeft: 20,
        marginBottom: 20
    },
    subtitle: {
        fontFamily: commonStyles.fontFamily,
        color: commonStyles.colors.secondary,
        fontSize: 20,
        marginLeft: 20,
        marginBottom: 30
    },
    iconBar: {
        flexDirection: 'row',
        marginHorizontal: 20,
        justifyContent: 'flex-end',
        marginTop: Platform.OS === 'ios' ? 40 : 10
    },
    addButton: {
        position: 'absolute',
        right: 30,
        top: 90,
        width: 50,
        height: 50,
        borderRadius: 25,
        backgroundColor: commonStyles.colors.today,
        justifyContent: 'center',
        alignItems: 'center'
    }
});