import React, { useState, useEffect } from 'react'
import { Image, FlatList, TouchableOpacity } from 'react-native'
import { Container, Icon, Button, Text, View } from 'native-base';
import FooterTabs from '../components/FooterTabs';
import * as Font from 'expo-font';
import commonStyles from '../commonStyles'
import { getLoggedUser } from '../controllers/Loginctrl'
import { callGetPurchases, checkSendPurchasesPraeServer } from '../controllers/Produtosctrl'
import Loading from '../components/Loading';
import HeaderComprae from '../components/HeaderComprae';
import imgCesta from '../../assets/imgs/cesta.jpeg'
const MeusPedidos = ({ navigation }) => {
    console.log('MEUS PEDIDOS XXXXXXXXX')
    // console.log(props)
    const [fontLoaded, setFontLoaded] = useState(false);
    const [user, setUser] = useState(null);
    const [pedidos, setPedidos] = useState(null);
    const [sincronizando, setSincronizando] = useState(false);
    let carregarFonts = async () => {
        await Font.loadAsync({
            Roboto: require("native-base/Fonts/Roboto.ttf"),
            Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
            Lato: require("../../assets/fonts/Lato.ttf")
        });
        setFontLoaded(true);
    }
    useEffect(() => {
        carregarFonts();
    }, []);
    if (!fontLoaded) {
        return <Loading></Loading>
    }

    let loadUser = async (force) => {
        if (user === null || force) {
            let usr = await getLoggedUser()
            setUser(usr)
        }
    }
    loadUser()
    //CARREGAR LISTA DE ENDERECOS

    let loadPurchases = async () => {
        if (navigation && navigation.state && navigation.state.params
            && navigation.state.params['refresh'] === true) {
            console.log('PEDIDOS XXXXXXXXXXXXXXXXXXXXXXXXXXXXX')
            let purchases = await callGetPurchases();
            console.log(purchases)
            navigation.state.params['refresh'] = false
            setPedidos(purchases)
        }
    }
    if (!sincronizando) {
        setSincronizando(true)
        setTimeout(() => checkSendPurchasesPraeServer(), 2000)
    }
    console.log('PEDIDO INTEIRO')
    loadPurchases()
    if (!pedidos) {
        return <Loading></Loading>
    }
    let jsxPedidos = (<FlatList data={pedidos}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item }) => {
            var d = '';
            if(item.datahoraabertura)
            {
                d = new Date(item.datahoraabertura);
                d = d.getDate() + "/" + (d.getMonth().length > 1 ?d.getMonth() : '0'+d.getMonth()) + "/" + d.getFullYear()
            }
            return (
                <View style={{
                    flexDirection: 'row', borderColor: '#AAA', borderBottomWidth: 1,
                    paddingVertical: 10, backgroundColor: '#FFF'
                }}>

                    <Image
                        source={imgCesta}
                        style={{ marginLeft: 10, width: 60, height: 60, borderRadius: 75 }}
                    />
                    <View style={{ flex: 6, marginRight: 45, marginLeft: 10, top: 2, borderColor: 'black', justifyContent: 'space-between' }}>
                        <Text style={{ marginLeft: 15, fontSize: 15, textAlign: 'center', fontWeight: 'bold' }}>{item.mercado.fantasia} </Text>
                        <Text style={{ marginLeft: 15, fontSize: 12, textAlign: 'center' }}>{d} </Text>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginRight: 25, marginLeft: 15, marginTop: 15 }}>
                            <Text style={{ fontSize: 15 }}>{item.vendaitens.length} {(item.vendaitens.length <= 1) ? 'Item' : 'Itens'} </Text>
                            <Text style={{ fontSize: 16, fontWeight: 'bold' }}>R$ {item.valortotal.toFixed(2).toString().replace('.', ',')}</Text>
                        </View>
                    </View>

                    <TouchableOpacity style={{
                        position: 'absolute', right: 7, alignSelf: 'center', width: 50, height: 50, borderRadius: 25,
                        backgroundColor: 'white', justifyContent: 'center', alignItems: 'center'
                    }}
                        activeOpacity={0.7}
                        onPress={async () => {
                            navigation.navigate('DetalhesCompra', { pedido: item })
                        }}>
                        <Icon name="md-more" size={50} style={{ color: commonStyles.colors.buttonGreen, fontSize: 40 }} />
                    </TouchableOpacity>
                </View>
            );
        }}>
    </FlatList>);
    if (pedidos.length <= 0) {
        jsxPedidos = (<View style={{ flexDirection: 'column', alignSelf: 'center', flex: 10 }}>
            <View style={{ flex: 10 }}></View>
            <View style={{ flex: 1, flexDirection: 'row' }}>
                <Icon name='alert'></Icon>
                <Text style={{ marginLeft: 10 }}>Você não realizou uma compra ainda!</Text>
            </View>
            <View style={{ flex: 10 }}></View>
        </View>)
    }
    console.log(pedidos)
    return (
        <Container>
            <View style={{ flex: 1 }}>
                <HeaderComprae hidelogo={true} showbackicon={true} navigation={navigation}
                    title='MEUS PEDIDOS'
                ></HeaderComprae>
                <View style={{ flex: 8 }}>
                    {jsxPedidos}
                </View>
            </View>
            <FooterTabs navigation={navigation}></FooterTabs>
        </Container >
    )
}
MeusPedidos.navigationOptions = {
    title: 'MeusPedidos',
    headerShown: false
}
export default MeusPedidos