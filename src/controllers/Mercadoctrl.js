import { AsyncStorage } from 'react-native';
import { getUrlNode } from './Loginctrl';
export const getSelectedMarket = async () => {
    let market = null;
    try {
        await AsyncStorage.getItem('market', (err, result) => {            
            if (result) {
                market = JSON.parse(result);
            }
        })
    } catch (error) {
        console.log('ERROR')
        console.log(error)
    }    
    return market;
}

export const getMarketList = async () => {
    try {        
        var arrMercados = []
        console.log(getUrlNode() + '/comprae/marketlist')
        const response = await fetch(getUrlNode() + '/comprae/marketlist')
        // console.log(response)
        let json = await response.json();   
        if(json.categoria === 'sucesso')
        {
            arrMercados = json.data
        }
    } catch (error) {
        console.log(error)    
    }

    return arrMercados;
}
export const getStatusMarket = async (market) => {
    try {    
        console.log('getStatusMarket')        
        let response = await fetch(market.homepage + '/filial/getfilial')
        if(response)
        {
            let json = await response.json();
            return json;
        }                
        return false
    } catch (error) {
        // console.log(error)
        return false
    }

    return true;
}