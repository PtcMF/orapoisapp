import { AsyncStorage } from 'react-native';
import { showMessage } from "../components/Utils";
import { getSelectedMarket } from "./Mercadoctrl"
import { getUrlNode } from './Loginctrl';

export const buscarProdutos = async (filtro) => {
    console.log('buscarProdutos');
    console.log(filtro);
    try {

        let market = await getSelectedMarket();
        console.log(market)
        var arrProdutos = []
        const response = await fetch(market.homepage + '/produto/buscarProdutoItem.gson/' + filtro + '/50')
        console.log('VERIFICAR RESPOSTA FETCH 5')
        let json = await response.json();
        console.log(json)
        if(json.categoria && json.categoria === 'erro')
        {
            return []
        }
        arrProdutos = json        
    } catch (error) {
        console.log(error)
    }

    return arrProdutos;
}

export const clearCarrinho = async () => {
    console.log('clearCarrinho')
    try {
        AsyncStorage.setItem('carrinho', JSON.stringify({}));
        // AsyncStorage.setItem('user', JSON.stringify({}));        
    } catch (error) {
        console.log('ERROR')
        console.log(error)
    }
}

export const enviarPedido = async (pedido, pagamento, retirarEstabelecimento, user) => {
    console.log(' enviarPedido');
    try {
        let address = user.selectedAddress
        let produtos = pedido.produtos
        if (!address) {
            showMessage('SELECIONE UM ENDEREÇO')
            return false
        }

        pedido['valordesconto'] = 0.0
        pedido['valordescontoporc'] = 0.0
        pedido['observacao'] = "Pedido realizado pelo APP!"
        pedido['tipovenda'] = "APP"
        pedido['retirar'] = retirarEstabelecimento
        pedido['cliente'] = {
            pessoa: {
                nome: user.nome,
                cnpjf: user.cnpjf,
                // rg: '169.443.9',
                // sexo: 'M',
                celular: user.celular,
                telefone: '(00) 0000-0000',
                email: user.email,
                tipopessoa: 'F',
                // indicadorinscest: 9,
            },
            limite: 0
        }
        if (address) {
            pedido.cliente.pessoa.numero = address.numero
            pedido.cliente.pessoa.endereco = {
                cidade: {
                    idcidade: address.city.idcidade,
                    uf: {
                        uf: address.uf.uf,
                    },
                },
                // cep: '85.670-000',
                endereco: address.endereco + ', ' + address.numero + ' - ' + address.referencia + ", " + address.complemento,
                bairro: address.bairro,
            }
        }
        pedido['status'] = 'A'

        let vendaitens = []
        produtos.forEach(element => {
            let vendaitem = {
                quantidade: element.quantidade,
                valorunitario: element.precovarejo,
                valortotal: (element.quantidade * element.precovarejo).toFixed(2),
                taxadesconto: 0.00,
                desconto: 0.00,
                codbarra: element.codbarra,
                subdescricao: element.subdescricao,
                filialitem: {
                    idfilialitem: element.idfilialitem
                }
            }
            vendaitens.push(vendaitem)
        });
        let totalpagamentos = []
        produtos.forEach(element => {
            let pagamentovenda = {
                descpagamento: pagamento,
                valor: (element.quantidade * element.precovarejo).toFixed(2),
            }
            totalpagamentos.push(pagamentovenda)
        });
        pedido['vendaitens'] = vendaitens
        pedido['totalpagamentos'] = totalpagamentos
        console.log(pedido)
        let data = {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'body': JSON.stringify(pedido)
            }
        }

        let market = await getSelectedMarket();
        pedido['mercado'] = market
        if (market.idcidade != address.city.idcidade) {
            console.log(market)
            showMessage('este mercado não faz entregas em outras cidades')
            return false
        }
        console.log('COMUNICAR COM COLISEU')
        console.log(market.homepage + '/venda/novaVendaApp')
        let result = await fetch(market.homepage + '/venda/novaVendaApp', data).then((response) => response.text())
        let retorno = JSON.parse(result)
        console.log(retorno)
        if (retorno.categoria === 'erro') {
            showMessage(retorno.mensagem, retorno.categoria)
            return false
        }
        let vendaRegistrada = JSON.parse(retorno.mensagem)
        console.log(vendaRegistrada)
        console.log(vendaRegistrada.datahoraabertura)
        pedido.datahoraabertura = vendaRegistrada.datahoraabertura
        callPutPurchases(pedido)
    } catch (error) {
        console.log('error1')
        console.log(error)
        showMessage('Não foi possível finalizar sua compra')
        return false
    }

    return true;
}
//PEGA LISTA DE ITENS NO CARRINHO
export const getCartProducts = async () => {
    let carrinho = [];
    await AsyncStorage.getItem('carrinho', (err, result) => {
        if (result) {
            carrinho = JSON.parse(result);
        }
    })
    return carrinho;
}

//ENVIA COMPRA PARA SERVIDOR APLICATIVO
export const callSendPurchasePraeServer = async (purchase) => {
    console.log('callSendPurchasePraeServer')
    return await callHTTP(getUrlNode() + '/comprae/savePurchase', purchase)
}
//ENVIA COMPRA PARA SERVIDOR APLICATIVO
export const checkSendPurchasesPraeServer = async () => {
    console.log('checkSendPurchasesPraeServer')
    let purchases = await callGetPurchases()
    console.log(purchases)
    if (purchases && purchases.length > 0) {
        for (let i = 0; i < purchases.length; i++) {
            if (!purchases[i].idvenda) {
                let purchaseServer = await callSendPurchasePraeServer(purchases[i])
                console.log('purchaseServer')
                console.log(purchaseServer)
                if (purchaseServer) {
                    purchases[i].idvenda = purchaseServer.idvenda;
                }
            }
        }
        await AsyncStorage.setItem('purchases', JSON.stringify(purchases));
    }

}
//PEGA LISTA DE COMPRAS REALIZADAS
export const callGetPurchases = async (user) => {
    let purchaes = [];
    await AsyncStorage.getItem('purchases', (err, result) => {
        if (result) {
            purchaes = JSON.parse(result);
        } else {
            purchaes = []
        }
    })
    return purchaes
}
//SALVA COMPRA NA LISTA DE COMPRAS REALIZADAS
export const callPutPurchases = async (purchase) => {
    console.log('callPutPurchases')
    let purchases = await callGetPurchases();
    if (!purchases) {
        purchases = []
    }
    purchases.push(purchase)
    await AsyncStorage.setItem('purchases', JSON.stringify(purchases));
}
//CHAMADA HTTP GENERICA
export const callHTTP = async (url, _data) => {
    console.log('callHTTP')
    console.log(url)
    console.log(_data)
    let data = {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'body': JSON.stringify(_data)
        }
    }

    try {
        let result = await fetch(url, data).then((response) => response.text())
        let retorno = JSON.parse(result)
        console.log(retorno)
        if (retorno.categoria === 'erro') {
            showMessage(retorno.mensagem, retorno.categoria)
            return null
        }
        return retorno.data
    } catch (error) {
        console.log('error')
        console.log(error)
        return null
    }
    return null
}