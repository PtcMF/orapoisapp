import { AsyncStorage } from 'react-native';
import { saveLoggedUser, getLoggedUser, urlNode } from '../controllers/Loginctrl';
import { showMessage } from '../components/Utils';

export const getUFs = async () => {
    console.log('buscar UFS');
    var ufs = []
    try {

        const response = await fetch(urlNode + '/uf/list')
        let json = await response.json();
        if (json.success === true) {
            ufs = json.data
        }
        ufs.unshift({ "nomeuf": "SELECIONE", "uf": "0", });
    } catch (error) {
        showMessage('Não foi possível conectar com servidor');
        console.log(error)
    }
    return ufs
}
export const getCities = async (uf) => {
    console.log('buscar Cidades ' + uf);
    var cities = []
    try {
        const response = await fetch(urlNode + '/cidade/findbycoduf/' + uf)
        let json = await response.json();
        console.log(json)
        if (json.success === true) {
            cities = json.data
        }
        cities.unshift({ "cidade": "SELECIONE", "idcidade": "0", "uf": "0" });
    } catch (error) {
        showMessage('Não foi possível conectar com servidor');
    }
    // console.log(cities)
    return cities;
}
export const callEditAddress = async (address, navigation, refreshFunction) => {
    console.log(address)
    console.log(navigation)
    AsyncStorage.setItem('editAddress', JSON.stringify(address));
    navigation.navigate('Endereco', { refresh: refreshFunction })
}
export const callDeleteAddress = async (tmpAddress, user) => {
    let tmpUser = JSON.parse(JSON.stringify(user))
    if (user.addresses) {
        if (user.addresses.length > 1) {
            for (var i = 0; i < user.addresses.length; i++) {
                if (user.addresses[i].id === tmpAddress.id) {
                    tmpUser.addresses.splice(i, 1)
                    break
                }
            }
            await saveLoggedUser(tmpUser)
        } else {
            showMessage('É preciso manter ao menos um endereço')
        }
    }
}
export const callSelectedAddress = async (address) => {
    let userLogged = await getLoggedUser();
    userLogged.selectedAddress = address
    console.log('callSelectedAddress')
    console.log(userLogged)
    saveLoggedUser(userLogged)
}
export const getEditAddress = async () => {
    let res = null;
    try {
        await AsyncStorage.getItem('editAddress', (err, result) => {
            if (result) {
                res = JSON.parse(result);
            }
        })

    } catch (error) {
        console.log('ERROR')
        console.log(error)
    }
    return res;
}
export const sendAddressServer = async (user, address) => {
    console.log('sendAddressServer')
    address.idcidade = address.city.idcidade
    address.idpessoa = user.idpessoa
    var tmpAddress = JSON.stringify(address)
    tmpAddress = JSON.parse(tmpAddress)
    tmpAddress.endereco = tmpAddress.endereco + '#' + tmpAddress.numero + '#' + '#' + tmpAddress.referencia + tmpAddress.complemento
    let data = {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'body': JSON.stringify({ address: tmpAddress })
        }
    }

    try {
        let result = await fetch(urlNode + '/comprae/saveaddress', data).then((response) => response.text())
        let retorno = JSON.parse(result)
        console.log('sendAddressServer result')
        console.log(retorno)
        if (retorno.categoria === 'erro') {
            showMessage(retorno.mensagem, retorno.categoria)
            return false
        }
        address.idend = retorno.data.idend
        return address
    } catch (error) {
        console.log('sendAddressServer error')
        console.log(error)
        return false
    }
    return false;
}

export const getServerAddresses = async (user) => {
    console.log(urlNode + '/comprae/findAddressesByUser/' + user.idpessoa)
    try {
        console.log('getServerAddresses: ')
        let result = await fetch(urlNode + '/comprae/findAddressesByUser/' + user.idpessoa).then((response) => response.text())
        let resp = JSON.parse(result)
        console.log(resp)
        if (resp.success === true) {
            let enderecos = resp.data
            for (let i = 0; i < enderecos.length; i++) {
                let endereco = enderecos[i]
                endereco.city = endereco.cidade
                endereco.numero = endereco.endereco.split('#')[1]
                endereco.referencia = endereco.endereco.split('#')[2]
                endereco.complemento = endereco.endereco.split('#')[3]
                endereco.endereco = endereco.endereco.split('#')[0]
                endereco.id = endereco.idend
                endereco.uf = endereco.cidade.iduf
            }
            return enderecos
        }
        return false
    } catch (error) {
        console.log('getServerAddresses: error')
        console.log(error)
    }
    return false;
}