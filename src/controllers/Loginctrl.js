import { AsyncStorage } from 'react-native';
import { showMessage } from '../components/Utils';
// export const urlNode = 'http://192.168.0.103:3000'
export const urlNodeTeste = 'http://201.33.225.214:8091'
export const urlNode = 'http://45.132.241.223:3000'
var tryChangeServer = 0

export const changeServer = () => {
    tryChangeServer++
    console.log(`tryChangeServer ` + tryChangeServer + ' - ' + urlNode)
    if(tryChangeServer > 5)
    {
        return true
    }
    return false
}
export const getUrlNode = () =>
{
    if(tryChangeServer > 5)
    {
        return urlNodeTeste
    }
    return urlNode
}
export const verificarCredenciais = async (email, navigation) => {
    let data = {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'body': JSON.stringify({ email: email })
        }
    }
    console.log('verificarCredenciais: ' + data)
    try {
        let result = await fetch(getUrlNode() + '/pessoa/requestEmailConfirmation', data).then((response) => response.text())
        let resp = JSON.parse(result)
        if (resp.success === true) {
            return true
        }
        return false
    } catch (error) {
        alert(error)
    }
    console.log('verificar credenciais')
    return false;
}
export const getCredencialByEmail = async (email) => {    
    console.log('getCredencialByEmail: ')
    try {
        let result = await fetch(getUrlNode() + '/pessoa/findbyemail/'+email).then((response) => response.text())        
        let resp = JSON.parse(result)
        console.log(resp)
        if (resp.success === true) {
            return resp.data            
        }
        return false
    } catch (error) {
        console.log(error)
    }
    return null;
}
export const sendProfileServer = async (user) => {    
    console.log('sendProfileServer')
    let data = {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'body': JSON.stringify({user:user})
        }
    }

    try {
        let result = await fetch(getUrlNode() + '/comprae/saveuser', data).then((response) => response.text())
        let retorno = JSON.parse(result)
        console.log(retorno)
        if(retorno.categoria === 'erro')
        {
            showMessage(retorno.mensagem, retorno.categoria)
            return false
        }        
    } catch (error) {
        console.log('error')
        console.log(error)
        return false
    }
    return true;
}
export const confirmEmail = async (codigo, email, navigation) => {
    let data = {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'body': JSON.stringify({ codigo: codigo })
        }
    }
    console.log(data)
    try {

        let result = await fetch(getUrlNode() + '/pessoa/emailConfirmation', data).then((response) => response.text());
        let resp = JSON.parse(result)
        console.log(resp)
        if (resp.success === true) {
            saveLoggedUser({ email: email })
            navigation.navigate('Profile')
        }else{
            showMessage(resp.data)
        }
    } catch (error) {
        console.log(error)
    }
    console.log('verificar credenciais')
    return false;
}
export const validateCPF = async (cpf) => {
    let data = {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'body': JSON.stringify({ cpf: cpf })
        }
    }
    console.log(data)
    try {

        let result = await fetch(getUrlNode() + '/pessoa/validatecpf/'+cpf, data).then((response) => response.text());
        let resp = JSON.parse(result)
        console.log(resp)
        if (resp.success === true) {
            if(resp.data.toString().indexOf('ok') > 1)
            {
                return true
            }
        }
    } catch (error) {
        console.log(error)
    }    
    return false;
}
export const getLoggedUser = async () => {
    let user = null;
    try {
        await AsyncStorage.getItem('user', (err, result) => {            
            if (result) {
                user = JSON.parse(result);
            }
        })

    } catch (error) {
        console.log('ERROR')
        console.log(error)
    }    
    console.log(user)
    return user;
}
export const saveLoggedUser = async (user) => {
    AsyncStorage.setItem('user', JSON.stringify(user));
    return user;
}
export const logout = async (navigation) => {
    AsyncStorage.setItem('user', JSON.stringify({}));
    AsyncStorage.setItem('carrinho', JSON.stringify({}));
    AsyncStorage.setItem('purchases', JSON.stringify([]));
    AsyncStorage.setItem('market', JSON.stringify({}));
    if(navigation)
    {
        navigation.navigate('Mercados')
    }
}
export const showProfileScreen = async (navigation) => {
    let user = await getLoggedUser()
    console.log('showProfileScreen')
    console.log(user)
    if(navigation)
    {
        if (!user.email) {
            navigation.navigate('Register')
        }else
        {
            navigation.navigate('Profile', { back: true })
        }
    }
}