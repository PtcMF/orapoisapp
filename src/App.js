import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import CarrinhoCompras from './screens/Carrinho'
import Endereco from './screens/Endereco';
import FinalizarCompra from './screens/FinalizarCompra'
import MeusEnderecos from './screens/MeusEnderecos';
import Mercados from './screens/Mercados';
import MeusPedidos from './screens/MeusPedidos';
import Produtos from './components/Produtos';
import Profile from './screens/Profile'
import Register from './screens/Register'
import SignIn from './screens/Login'
import DetalhesCompra from './screens/DetalheCompra';

const RootStack = createStackNavigator({
  Mercados: {
    screen: Mercados
  },
  MeusPedidos: {
    screen: MeusPedidos
  },
  DetalhesCompra: {
    screen: DetalhesCompra
  },
  Products: {
    screen: Produtos
  },
  MeusEnderecos: {
    screen: MeusEnderecos
  },
  Profile: {
    screen: Profile
  },
  Endereco: {
    screen: Endereco
  },
  Register: {
    screen: Register
  },
  FinalizarCompra: {
    screen: FinalizarCompra
  },
  Carrinho: {
    screen: CarrinhoCompras
  },
  SignIn: {
    screen: SignIn
  },

});

const App = createAppContainer(RootStack);
export default App;