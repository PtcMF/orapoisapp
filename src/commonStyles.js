export default {
     fontFamily: 'Segoe',
     fontFamilyBold: 'SegoeBold',
    colors: {
        today: '#B13B44',
        secondary: '#FFF',
        mainText: '#222',
        subText: '#555',
        grayBackground:'#EFEFEF',
        grayHeader:'#e3e3e3',
        buttonGreen:'#02AF9C',
        iconColor:'black'
    }

}