import {StyleSheet } from 'react-native'

export default StyleSheet.create({
    ex:{
        paddingHorizontal: 15,
        marginVertical: 5,
        borderRadius: 10,
        borderWidth: 2,
        borderColor: '#222',
        fontSize: 24,
        fontWeight: 'bold'
    },
    produtoStyle:
    {
        flex:4,
        paddingHorizontal: 15,
        height: 200,
        backgroundColor: '#DDD',
        borderWidth: 0.5,
        borderColor: '#222',
        flexDirection: 'column', 
        justifyContent: 'space-between'
    },
    menuicons:
    {
        height: 20,
        backgroundColor: 'blue',
        flexDirection: 'row', 
    },
    width100:
    {
        width: 100
    },
    width300:
    {
        width: 300
    },
    top10:
    {
        top : 10,
    },
    fontDefault:{
        fontFamily:'Segoe'
    }
})