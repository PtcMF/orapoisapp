import React from 'react'
import { View, Text, TouchableWithoutFeedback, Modal } from 'react-native'
import Loading from './Loading'

const Wait = ({ props, visible }) => {    
    return (
        <Modal transparent={true} visible={visible} animationType='slide' >
            <TouchableWithoutFeedback >
                <View style={{ flex: 10, backgroundColor: 'rgba(0, 0, 0, 0.7)', flexDirection: 'column', justifyContent:'space-between' }}>
                    <View></View>
                    <View style={{ backgroundColor: '#FFF' }}>
                        <Loading></Loading>
                    </View>
                    <View></View>
                </View>
            </TouchableWithoutFeedback>
        </Modal>)
}

export default Wait