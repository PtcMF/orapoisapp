import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import DefaultStyle from '../style/DefaultStyle'

export default function () {
  return (    
    <Text style={DefaultStyle.ex}>PRAESOFT</Text>    
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',    
  },
  title:{
    fontSize:28
  }
});
