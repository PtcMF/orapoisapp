const PraeValidation = {
    email: {
      presence: {
        message: '^Please enter an email address'
      },
      email: {
        message: '^Informe um e-mail válido'
      }
    },
    nome: {
      presence: {
        message: '^Insira um nome'
      },
      length: {
        minimum: 10,
        message: 'precisa ter pelo menos 10 caracteres'
      }
    },
    celular: {
      presence: {
        message: '^Insíra um número de celular'
      },
      length: {
        minimum:15,
        message: '^Número de celular inválido'
      }
    },
    
    password: {
      presence: {
        message: '^Please enter a password'
      },
      length: {
        minimum: 5,
        message: '^Your password must be at least 5 characters'
      }
    }
  }
  
  export default PraeValidation