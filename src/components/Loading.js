
import React from 'react'
import { View, Image } from 'react-native'
const Loading = () => {
    return (
        <View style={{ flex: 10, justifyContent: 'space-between' }}>
            <View></View>
            <Image
                source={{ uri: 'https://gifimage.net/wp-content/uploads/2017/10/carregando-gif-animado-3.gif' }}
                style={{ width: 120, height: 120, borderRadius: 15, alignSelf: 'center' }}
            />
            <View></View>
        </View>
    )
}

export default Loading