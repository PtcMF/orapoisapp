import React, { useState, useEffect } from 'react'
import { FlatList, SafeAreaView, Image, StatusBar } from 'react-native'
import { Container, Header, Text, View } from 'native-base';
import DefaultStyle from '../style/DefaultStyle'
import ProductCard from '../components/ProductCard'
import Findbar from '../components/Findbar'
import FooterTabs from '../components/FooterTabs';
import * as Font from 'expo-font';
import { buscarProdutos, getCartProducts } from '../controllers/Produtosctrl'
import { getSelectedMarket } from '../controllers/Mercadoctrl'
import Wait from './Wait';
import Loading from './Loading'
import { showMessage } from './Utils';
import HeaderComprae from './HeaderComprae';
import commonStyles from '../commonStyles';

const Produtos = ({ navigation }) => {
    console.log('TELA DE PRODUTOS')
    const [fontLoaded, setFontLoaded] = useState(false);
    const [produtos, setProdutos] = useState(null);
    const [showWait, setShowWait] = useState(false);
    const [isLoading, setIsLoading] = useState(true);
    const [carrinho, setCarrinho] = useState(null);
    const [market, setMarket] = useState(null);
    let searchProdutos = async (filtro) => {
        let prods = await buscarProdutos(filtro);
        setIsLoading(false)
        if (prods.length <= 0) {
            showMessage('NENHUM PRODUTO ENCONTRADO')
        } else {
            setProdutos(prods);
        }
    }
    let carregarFonts = async () => {
        await Font.loadAsync({
            Roboto: require("native-base/Fonts/Roboto.ttf"),
            Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
            Lato: require("../../assets/fonts/Lato.ttf")
        });
        setFontLoaded(true)
    }
    useEffect(() => {
        carregarFonts();
    }, []);
    if (!fontLoaded) {
        return <Loading></Loading>
    }
    if (isLoading === true) {
        searchProdutos('a')
    }
    verificarCarrinho = async (force) => {

        if (carrinho == null || force ||
            (navigation && navigation.state && navigation.state.params && navigation.state.params['refresh'] === true)) {
            let car = await getCartProducts();
            setCarrinho(car)
            if(navigation.state.params)
            {
                navigation.state.params['refresh'] = false
            }
        }
    }
    verificarCarrinho()
    loadMarket = async () => {
        console.log('BUSCAR MERCADO SELECIONADO')
        let mar = await getSelectedMarket()
        console.log(mar)
        setMarket(mar)
    }
    if (!market) {
        loadMarket()
        return <Loading></Loading>
    }
    let jsxProdutos;
    if (isLoading) {
        jsxProdutos = (<Loading></Loading>)
    } else {
        callbackSaveProduct = () =>
        {
            verificarCarrinho(true)
        }
        jsxProdutos = (<SafeAreaView style={DefaultStyle.top10, { flex: 9 }}>
            <FlatList data={produtos} keyExtractor={item => item.idfilialitem} numColumns={2}
                renderItem={({ item }) => {
                    return (<ProductCard {...item} callbackSaveProduct={callbackSaveProduct}></ProductCard>);
                }}
            />
        </SafeAreaView >)
    }
    console.log('CARRINHO')
    console.log(market)
    let jsxLogoMercado = (<Image style={{ height: 110, resizeMode: 'contain' }} source={{ uri: market.logo }} />)
    StatusBar.setBarStyle('dark-content', true);
    return (
        <Container style={{ backgroundColor: commonStyles.colors.grayBackground }}>
            <View style={{ height: 35, backgroundColor: commonStyles.colors.grayHeader }}></View>
            <View style={{ height: 130, justifyContent: 'center' }}>
                {jsxLogoMercado}
            </View>
            <Findbar searchEvent={searchProdutos} ></Findbar>
            {jsxProdutos}
            <Wait visible={showWait}></Wait>
            <FooterTabs navigation={navigation} carrinho={carrinho}></FooterTabs>
        </Container >
    )
}
Produtos.navigationOptions = {
    title: 'Produtos',
    headerShown: false
}
export default Produtos