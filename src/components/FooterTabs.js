import React from 'react';
import { Footer, FooterTab, Button, Icon, Badge, Text, View } from 'native-base';
import commonStyles from '../commonStyles';
import { getCartProducts } from '../controllers/Produtosctrl';
import { getLoggedUser } from '../controllers/Loginctrl';

const FooterTabs = ({ navigation, carrinho, user }) => {
  // const [verificado, setVerificado] = useState(false);
  let products = []
  console.log('FOOTER')
  let carrinhoBadge = (<Button onPress={() => navigation.navigate('Carrinho', { refresh: true })}><Icon name="md-cart" style={{ color: commonStyles.colors.iconColor }} /></Button>);
  if (carrinho && Object.entries(carrinho).length > 0) {
    carrinhoBadge = (<Button badge vertical onPress={() => navigation.navigate('Carrinho', { refresh: true })}>
      <Badge onPress><Text> {Object.entries(carrinho).length}</Text></Badge>
      <Icon name="md-cart" style={{ color: commonStyles.colors.iconColor }} />
    </Button>)
  }

  return (
    <Footer>
      <FooterTab style={{ backgroundColor: commonStyles.colors.grayHeader }}>
        <Button onPress={() => navigation.navigate('Products', { refresh: true })} >
          <Icon name="md-search" style={{ color: commonStyles.colors.iconColor }} />
        </Button>
        {carrinhoBadge}
        <Button onPress={async () => {
          let user = await getLoggedUser();
          if(user && user.email)
          {
            navigation.navigate('Profile', { back: true })
          }else{
            navigation.navigate('Register')
          }
        }}>
          <Icon name="md-person" style={{ color: commonStyles.colors.iconColor }} />
        </Button>
        <Button onPress={() => navigation.navigate('MeusPedidos', { refresh: true })}>
          <Icon name="ios-pricetags" style={{ color: commonStyles.colors.iconColor }} />
        </Button>
      </FooterTab>
    </Footer>
  )
}

export default FooterTabs