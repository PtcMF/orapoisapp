import React from 'react'
import { View, Text, Image, TouchableWithoutFeedback, TouchableOpacity, Modal } from 'react-native'
import DefaultStyle from '../style/DefaultStyle'
import Swipeable from 'react-native-gesture-handler/Swipeable'
import commonStyles from '../commonStyles'
import Icon from 'react-native-vector-icons/FontAwesome'
import { AsyncStorage } from 'react-native';
import { TouchableHighlight } from 'react-native-gesture-handler'
import { Button, Input, Item, Label } from 'native-base'
import Loading from './Loading'

const initialState = { quantidade: 0, modalVisible: false, obs : '' }
let produto;
class ProductCard extends React.Component {
    state = {
        ...initialState
    }
    getRightContent = () => {
        return (
            <TouchableOpacity style={{
                backgroundColor: 'red', flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end',
                paddingHorizontal: 20
            }}
                onPress={() => { this.props.onDelete(this.props.idfilialitem) }}>
                <Icon name="trash" size={30} color='#FFF' />
            </TouchableOpacity>
        )
    }
    onCancel = () => {
        console.log('CANCEL')
        this.setState({ modalVisible: false })
    }

    onSave = async () => {
        if (this.state.quantidade < 1) {
            alert('Quantidade Invalida')
            return
        }
        let id = this.props.idfilialitem
        let produto = Object.assign({}, this.props);
        console.log(produto)
        produto["quantidade"] = this.state.quantidade
        produto["obs"] = this.state.obs
        try {
            let strProd = JSON.stringify(produto)
            AsyncStorage.getItem('carrinho', (err, result) => {
                let carrinho = {};
                if (result) {
                    carrinho = JSON.parse(result);
                }                
                carrinho[id] = produto
                AsyncStorage.setItem('carrinho', JSON.stringify(carrinho));
                this.setState({ modalVisible: false, quantidade :  0})                
                if(this.props.callbackSaveProduct)
                {
                    this.props.callbackSaveProduct()
                }
            })

        } catch (error) {
            console.log('ERROR')
            console.log(error)
        }
    }
    cardProdutoCarrinho = () => {
        let img;
        img = (this.props.uri) ? this.props.uri : 'https://cdn3.iconfinder.com/data/icons/abstract-1/512/no_image-512.png';
        img = (this.props.uri) ? this.props.uri : 'http://201.33.225.214:8083/mediawiki/images/comprae/' + this.props.codbarra +'.png';
        if (this.state.quantidade === 0 && this.props.quantidade > 0) {
            this.setState({ quantidade: this.props.quantidade })
            return <Loading></Loading>
        }
        let card = <Swipeable renderRightActions={this.getRightContent}>
            <View style={{
                flexDirection: 'row', borderColor: '#AAA', 
                paddingVertical: 10, backgroundColor: '#FFF', borderRadius:10, margin:5
            }}>
                <Image
                    style={{ width: 80, height: 80, marginTop: 5 , resizeMode: 'contain'}}
                    source={{ uri: img }}
                />
                <View style={{ flex: 6, marginRight: 15, marginLeft: 10, top: 2, borderColor: 'black', borderWidth: 0, justifyContent: 'space-between' }}>
                    <Text style={{ fontFamily: 'Segoe', color: commonStyles.colors.mainText, fontSize: 11 }}>
                        {this.props.subdescricao}</Text>
                    <View flexDirection='row' style={{ paddingTop: 5, justifyContent: 'space-between' , borderTopWidth:1, borderColor:commonStyles.colors.buttonGreen }}>
                        <Text style={{ fontFamily: commonStyles.fontFamily,  fontSize: 20 }}>
                            R$ {(this.props.precovarejo * this.state.quantidade).toFixed(2).replace('.', ',')}</Text>
                        <View flexDirection='row' style={{ }}>
                            <Icon name="minus" size={25} color={commonStyles.colors.buttonGreen} style={{ marginLeft: 5 }}
                                onPress={() => {
                                    if (this.state.quantidade <= 1) {
                                        return;
                                    }
                                    let qtd = this.state.quantidade - 1
                                    this.props.updateQuantidade(this.props.idfilialitem, qtd)
                                    this.setState({ quantidade: qtd })
                                }} />
                            <Text style={{ marginLeft: 30, marginRight: 30, marginTop: -7, fontSize: 20, alignSelf: 'center' }}>{this.state.quantidade}</Text>
                            <Icon name="plus" size={25} color={commonStyles.colors.buttonGreen} style={{ marginRight: 5 }}
                                onPress={() => {
                                    let qtd = this.state.quantidade + 1
                                    this.props.updateQuantidade(this.props.idfilialitem, qtd)
                                    this.setState({ quantidade: qtd })
                                }} />
                        </View>
                    </View>
                </View>

            </View>
        </Swipeable>;
        return card;
    }
    cardProdutoFinalizar = () => {
        let img;
        console.log(this.props)
        img = (this.props.codbarra) ? 'http://201.33.225.214:8083/mediawiki/images/comprae/' + this.props.codbarra +'.png' : 'https://cdn3.iconfinder.com/data/icons/abstract-1/512/no_image-512.png';
        if (this.state.quantidade === 0 && this.props.quantidade > 0) {
            this.setState({ quantidade: this.props.quantidade })
        }
        let card =
            <View style={{
                flexDirection: 'row', borderColor: '#AAA', borderBottomWidth: 1,
                paddingVertical: 10, backgroundColor: '#FFF'
            }}>
                <Image
                    style={{ width: 90, height: 90, marginTop: 5, resizeMode: 'contain' }}
                    source={{ uri: img }}
                />
                <View style={{ flex: 6, marginRight: 15, marginLeft: 10, top: 2, borderColor: 'black', borderWidth: 0, justifyContent: 'space-between' }}>
                    <Text style={{ fontFamily: commonStyles.fontFamily, color: commonStyles.colors.mainText, fontSize: 14, fontWeight: 'bold' }}>
                        {this.props.subdescricao}</Text>
                    <View flexDirection='row' style={{ paddingTop: 30, justifyContent: 'space-between' }}>
                        <Text style={{ fontFamily: commonStyles.fontFamily, color: 'blue', fontSize: 22, fontWeight: 'bold' }}>
                            R$ {(this.props.precovarejo * this.state.quantidade).toFixed(2).replace('.', ',')}</Text>
                        <View flexDirection='row' style={{ borderWidth: 0, borderRadius: 5 }}>
                            <Text style={{ marginLeft: 35, marginRight: 35, marginTop: -7, fontSize: 27 }}>{this.state.quantidade}</Text>
                        </View>
                    </View>
                </View>
            </View>
        return card;
    }
    render() {
        
        this.produto = this.props;
        let img;
        img = (this.props.codbarra) ? 'http://201.33.225.214:8083/mediawiki/images/comprae/' + this.props.codbarra +'.png' : 'https://cdn3.iconfinder.com/data/icons/abstract-1/512/no_image-512.png';
        if (this.props.editar === false) {
            let card = this.cardProdutoFinalizar()
            return (card)
        }
        if (this.props.quantidade) {
            let card = this.cardProdutoCarrinho()
            return (card)
        }
        return (
            <View style={{
                flex: 4, height: 160, backgroundColor: 'white', margin: 7, borderRadius: 10, borderColor: '#222', flexDirection: 'column',
                justifyContent: 'space-between'
            }}>
                <Modal transparent={true} visible={this.state.modalVisible}
                    onRequestClose={this.onCancel}
                    animationType='slide'>
                    <TouchableWithoutFeedback
                        onPress={this.onCancel}>
                        <View style={{ flex: 1, backgroundColor: 'rgba(0, 0, 0, 0.7)' }}></View>
                    </TouchableWithoutFeedback>
                    <View style={{ backgroundColor: '#FFF', flex: 6, bottom: 0, justifyContent: 'space-between' }}>
                        <View style={{ height: 10 }}></View>
                        <Image
                            style={{ width: 180, height: 180, marginTop: 5, alignSelf: 'center', resizeMode: 'contain' }}
                            source={{ uri: img }}
                        />
                        <View>
                            <Text style={{ fontSize: 20, textAlign: 'center', fontFamily: commonStyles.fontFamily }}>{this.props.subdescricao}</Text>
                            <Text style={{ fontSize: 14, textAlign: 'center', fontFamily: commonStyles.fontFamily, marginTop: 10 }}>{this.props.codbarra}</Text>
                        </View>
                        <Item color="black" regular style={{ marginLeft: 10, marginRight: 10 , borderRadius:10}}>
                            <Label style={{marginLeft:5,  top: -17, fontSize:10 }}>Observação</Label>
                            <Input value={this.state.obs} style={{left:-60}}
                                onChangeText={(text) => { this.setState({ obs: text })}} />
                        </Item>
                        <View style={{ flexDirection: 'column' }}>
                            <View flexDirection='row' style={{ justifyContent: 'space-between', marginBottom: 10 }}>
                                <Text style={{ flex: 5, marginLeft: 15, fontSize: 30, fontFamily: commonStyles.fontFamily }}>R$ {Number(this.props.precovarejo).toFixed(2).replace(".", ",")}</Text>
                                <View style={{ flex: 3, flexDirection: 'row' }}>
                                    <Icon name="minus" size={30} color={commonStyles.colors.buttonGreen} onPress={() => {
                                        if (this.state.quantidade < 1) {
                                            return;
                                        }
                                        let qtd = this.state.quantidade - 1
                                        this.setState({ quantidade: qtd })
                                    }} />
                                    <Text style={{ marginLeft: 30, marginRight: 30, marginTop: -7, fontSize: 30 }}>{this.state.quantidade}</Text>
                                    <Icon name="plus" size={30} color={commonStyles.colors.buttonGreen} onPress={() => {
                                        let qtd = this.state.quantidade + 1
                                        this.setState({ quantidade: qtd })
                                    }} />
                                </View>
                            </View>

                            <Button style={{ height: 50, marginBottom: 5, marginLeft: 15, marginRight: 15, backgroundColor: commonStyles.colors.buttonGreen, justifyContent: 'center' }}
                                onPress={this.onSave}><Text style={{ textAlign: 'center', color:'white' }}>ADICIONAR AO CARRINHO</Text>
                            </Button>
                        </View>
                    </View>
                    <TouchableWithoutFeedback
                        onPress={this.onCancel}>
                        <View style={{ flex: 1, backgroundColor: 'rgba(0, 0, 0, 0.7)' }}></View>
                    </TouchableWithoutFeedback>
                </Modal>
                <TouchableOpacity style={{ justifyContent: 'space-between', flex: 10 }}
                    onPress={() => { this.setState({ modalVisible: true }) }}>
                    <Image
                        style={{ width: 60, height: 60, marginTop: 5, alignSelf: 'center', resizeMode: 'contain' }}
                        source={{ uri: img }}
                    />
                    <Text style={{ fontSize: 13, textAlign: 'center', fontFamily: commonStyles.fontFamily }}>{this.props.subdescricao}</Text>
                    <Text style={{ marginTop: 5, fontSize: 25, textAlign: 'center', fontFamily: commonStyles.fontFamily }}>R$ {Number(this.props.precovarejo).toFixed(2).replace(".", ",")}</Text>
                </TouchableOpacity>
            </View>
        );
    }

}

export default ProductCard