import React from 'react'
import Toast from 'react-native-root-toast';
import { View, Text, TouchableWithoutFeedback, Modal, Image } from 'react-native'
export const showMessage = async (text, categoria) => {
    let toast = Toast.show(text, { duration: 3000, backgroundColor: 'orange', shadow: false, animation: true });
}
export const ShowMessage1 = ({ props, visible, timeout }) => {
    return (
        <Modal transparent={true} visible={visible} animationType='slide' >
            <TouchableWithoutFeedback >
                <View style={{ flex: 10, backgroundColor: 'rgba(0, 0, 0, 0.9)', flexDirection: 'row', justifyContent: 'space-between' }}>
                    <View></View>
                    <View style={{ flexDirection: 'column', justifyContent: 'space-between' }}>
                        <View></View>
                        <View>
                            <Image
                                style={{ width: 160, height: 160, marginTop: 5, resizeMode: 'contain' }}
                                source={{ uri: 'https://lh3.googleusercontent.com/proxy/CSvhavcJnOJOvBKAK0RI2QqmTphYBRwdsuDknpnnzINe5MMV03Z6qyTCP5X3U2qPKMvH_54Wu3gBVaJ1gAmlMHIJmPHkie-zz5BqJOZp8AlNckUy6iKI4RrK9Ts' }}
                            />

                            <Text style={{alignSelf:'center', fontSize:16, color:'white'}}>PEDIDO REALIZADO!</Text>
                        </View>
                        <View></View>
                    </View>
                    <View></View>

                </View>
            </TouchableWithoutFeedback>
        </Modal>)
}