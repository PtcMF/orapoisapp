import React, { useState, useEffect } from 'react'
import { Image, StatusBar } from 'react-native'
import { View, Text, Icon, Button } from 'native-base';
import * as Font from 'expo-font';
import { FlatList } from 'react-native-gesture-handler';
import commonStyles from '../commonStyles';
import logoComprae from '../../assets/imgs/logo_comprae.png'


const HeaderComprae = ({ navigation, icons, backgroundColor, fontfamily, container, showbackicon, hidelogo, title }) => {    
    StatusBar.setBarStyle('dark-content', true);

    container = (container) ? container : (
        <View style={{ alignSelf: 'center', flex: 6 }}>
            <Text style={{ alignSelf: 'center', fontFamily: (fontfamily) ? fontfamily : '' }} >{title}</Text>
        </View>
    )
    return (
        <View>
            <View style={{ height: 32, backgroundColor: commonStyles.colors.grayHeader, color: 'black' }} >
                <StatusBar translucent backgroundColor={backgroundColor} color='black' />
            </View>
            <View style={{ height: 60, backgroundColor: (backgroundColor) ? backgroundColor : '#fbfbfb', flexDirection: 'row', justifyContent: 'space-between' }}>
                <View style={{ alignSelf: 'center', flex: 2, marginLeft: 10 }}>
                    {(hidelogo) ? null :
                        <Image source={logoComprae}
                            style={{ marginLeft: 15, width: 70, height: 30, alignSelf: 'center', resizeMode: 'contain'}}
                        />
                    }
                    {(!showbackicon) ? null :
                        <Icon name={'ios-arrow-back'} size={30} color='white' style={{ marginLeft: 7 }} onPress={() => navigation.goBack()} />
                    }
                </View>
                {container}
                <View style={{ flexDirection: 'row', alignSelf: 'center', justifyContent: 'flex-end', flex: 2 }}>
                    <FlatList data={icons} horizontal={true} contentContainerStyle={{ flexGrow: 1, justifyContent: 'flex-end' }}
                        keyExtractor={item => item.name}
                        renderItem={({ item }) => {
                            return (<Icon name={item.name} size={30} color='white' style={{ marginRight: 15 }} onPress={item.onPress} />);
                        }}
                    />
                </View>
            </View>
        </View>
    )
}
export default HeaderComprae