import React, { Component, useState , useEffect } from 'react'
import { Header, Item, Input, Icon, Button, Text, View } from 'native-base';
import * as Font from 'expo-font';
import commonStyles from '../commonStyles';

export function onFilter({ filter }) {
    // console.log('onFilter')
    return { header: null }
}

const Findbar = ({props, searchEvent}) => {

    const [filter, setFilter] = useState('');
    return (
        <Header searchBar rounded noShadow style={{backgroundColor:commonStyles.colors.grayBackground}}>
            <Item>
                <Icon name="ios-search" />
                <Input
                    onChangeText={text => setFilter(text)}
                    onBlur={(e) => searchEvent(filter)}
                    placeholder="Search" />
                {/* <Icon name="ios-checkmark-circle-outline" /> */}
            </Item>
            {/* <Button transparent>
                <Text>Search</Text>
            </Button> */}
        </Header>
    )
}

export default Findbar;